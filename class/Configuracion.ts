export class Configuracion {
  id: string;
  estado: number;
  municipio: number;
  lat: number;
  lon: number;
  facebook: string;
  kayak: string;
  twitter: string;
  url: string;
  location: string;
  localidad: string;
  flickr: string;
  inegi: string;
  trivago: string;
  instagram: string;
  busbuster: string;
  miEscape: string;
  youtube: string;
  ambiguo:boolean;
	tripadvisor: string;
  activo: boolean;
  cuentaOficialYoutube: string;
  trends: string;
  bestday: string;
  estadoNombre: string;
  municipioNombre: string;
  webOficial: string;

busbusterDestino: string[];
miescapeDestino: string[];
blablacarDestino: string[];

  nombre: string;
  acronimo: string;
  clave: string[];

  tipoDestino:number;
}