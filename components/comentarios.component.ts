import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { IndicadorService } from '../services/indicadores.service';
import { Response } from '@angular/http';
import { IMyOptions, IMyDateModel } from 'mydatepicker';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'comentarios',
  templateUrl: '../templates/comentarios.component.html',
})

export class ComentariosComponent implements OnInit {
  initialDate: Date;
  finalDate: Date;
  filter: string = undefined;
  active: number;
  selectedSource = "Todas las fuentes";
  sentimiento = ["Positivo", "Neutro", "Negativo"];

  fuentes = [
    "Todas las fuentes",
    "TripAdvisor",
    "Instagram",
    "airbnb",
    "youtube",
    "booking",
    "flickr",
    "twitter",
    "foursquare",
    "Yelp"];

  iconos = [
    "fa fa-2x fa-list",
    "fa fa-2x fa-tripadvisor ",
    "fa fa-2x fa-instagram",
    "fa fa-2x fa-home",
    "fa fa-2x fa-youtube ",
    "fa fa-2x fa-book",
    "fa fa-2x fa-flickr ",
    "fa fa-2x fa-twitter",
    "fa fa-2x fa-foursquare",
    "fa fa-2x fa-yelp "];

  idDestino: string;
  sub: any;
  comentarios: Object[] = [];

  selectComment: any = {};
  posComment: number;

  showComentario(id: any): void {
    if (id == undefined || id == null)
      return;

    this.posComment = this.buscarComentario(id);
    this.selectComment = this.comentarios[this.posComment];
  }

  buscarComentario(id: any): number {
    for (var i = 0; i < this.comentarios.length; i++) {
      if (this.comentarios[i]["id"] == id)
        return i;

    }
    return null;
  }

  constructor(private router: ActivatedRoute, private indicadorService: IndicadorService) {
    this.idDestino = router.snapshot.params['id'];

  }


  ngOnInit(): void {

    this.sub = this.router.params.subscribe((params: Params) => {

      this.idDestino = params['id'];

      if (this.idDestino != null || this.idDestino != undefined)

        this.indicadorService.getComments(this.idDestino).then(data => {
          $('#preloader').delay(0).fadeIn('fast');
          $('#status').fadeIn('fast');
          this.comentarios = data;

          $('#status').fadeOut(550);
          $('#preloader').delay(550).fadeOut('fast');
        });

    });
  }



  getCommentsDate() {
    if (this.initialDate == undefined || this.finalDate == undefined)
      return;

    else {
      $('#preloader').delay(0).fadeIn('fast');
      $('#status').fadeIn('fast');
      return this.indicadorService.getCommentByDate(this.idDestino, this.initialDate.getTime() / 1000, this.finalDate.getTime() / 1000).then(data => {

        this.comentarios = data;

        $('#status').fadeOut(550);
        $('#preloader').delay(550).fadeOut('fast');
      });
    }
  }

  setFave() {
    if (this.selectComment == undefined || this.selectComment == null)
      return;

    this.indicadorService.setFave(this.selectComment.id).then(data => {

      this.comentarios[this.posComment]["fav"] = 1;

    });

  }

  setRead() {


    if (this.selectComment == undefined || this.selectComment == null || this.selectComment["read"] == 1)
      return;
    else
      this.indicadorService.setRead(this.selectComment.id).then(data => {

        this.comentarios[this.posComment]["read"] = 1;
        this.comentarios[this.posComment]["comment_new"] = 0;


      });
  }

  setDeleted() {
    if (this.selectComment == undefined || this.selectComment == null || this.selectComment["trash"] == 1)
      return;
    else

      this.indicadorService.setDeleted(this.selectComment.id).then(data => {


        this.comentarios[this.posComment]["trash"] = 1;
        this.comentarios[this.posComment]["fav"] = 0;


      });
  }


  setPositive() {
    if (this.selectComment == undefined || this.selectComment == null || this.selectComment["sentimient"] == 1)
      return;
    else
      this.indicadorService.setPositive(this.selectComment.id).then(data => {

        this.comentarios[this.posComment]["sentimient"] = 1;


      });
  }

  setNegative() {

    console.log(this.selectComment);

    if (this.selectComment == undefined || this.selectComment == null || this.selectComment["sentimient"] == 2)
      return;
    else

      this.indicadorService.setNegative(this.selectComment.id).then(data => {

        this.comentarios[this.posComment]["sentimient"] = 2;

      });
  }

  setNeutral() {
    if (this.selectComment == undefined || this.selectComment == null || this.selectComment["sentimient"] == 0)
      return;
    else
      this.indicadorService.setNeutral(this.selectComment.id).then(data => {
        this.comentarios[this.posComment]["sentimient"] = 0;
      });
  }



  setCss(sentimient) {
    if (sentimient == 1) { //verde
      return 'verdeBg';
    } else if (sentimient == 0) { //naranja
      return 'naranjaBg';
    } else if (sentimient == 2) { //rojo      
      return 'rojoBg';
    }
    else
      return '';
  }

  truncate(text: any) {

    if (text == null || text == undefined)
      return;

    if (text.length > 140)
      return text.substring(0, 140) + "...";
    else
      return text;
  }


  getIconoFuente(source) {

    for (var i = 0; i < this.fuentes.length; i++)
      if (source == this.fuentes[i])
        return this.iconos[i];

  }

  setTooltip(tag) {
    for (var key in this.tabs) {

      if (this.tabs.hasOwnProperty(key))

        if (key == tag)
          return this.tabs[key];
    }
  }

  getIcono(tag) {
    if (tag != undefined || tag != null)
      return "./resources/img/iconos/" + tag + ".png";
  }

  changeFilter(filter) {

    if (this.filter != filter) {
      this.active = 1;
      this.filter = filter;
    } else {

      if (this.active == 0)
        this.active = 1;
      else
        this.active = 0;

    }

  }

  tabs = {
    mediaOferta: "Promedio General Oferta",
    cia: "Calidad de la infraestructura de acceso",
    ca: "Calidad de los atractivos",
    ca1: "Calidad de alojamiento",
    ca2: "Calidad de alimentos",
    esd: "Estado de los señalamientos para llegar al destino",
    egct: "Estado general de la calidad del transporte",
    feh: "Facilidad de encontrar el hospedaje",
    fea: "Facilidad de encontrar hospedaje (alternativo)",
    rcp: "Relación calidad precio",
    va: "Variedad de los atractivos",
    ph: "Precio Hoteles",
    pr: "Precio Restaurantes",
    pt: "Precio Transporte",
    mediaDemanda: "Promedio General Demanda",
    cat: "Relevancia de la atractividad",
    ata: "Porcentaje de visitantes extranjeros",
    ep: "Estadía promedio",
    mediaSeguridad: "Promedio General Seguridad",
    ps: "Percepción de seguridad",
    rn: "Riesgos naturales",
    mediaMercadotecnea: "Promedio General Mercadotécnia",
    ed: "Experiencia diferente",
    dit: "Disponibilidad de información turística",
    ev1: "Experiencia de viaje",
    ev2: "Emoción al visitar",
    pp: "Percepción de la promoción",
    mediaAccesibilidad: "Promedio General Accesibilidad",
    ci: "Canales de información",
    cc: "Canales de comunicación",
    st: "Sistema de transporte",
    ad: "Accesibilidad 'discapacidad'",
    ef: "Entorno físico",
    mediaComunidad: "Promedio General Comunidad",
    e: "Empleo",
    h: "Hospitalidad",
    vs1: "Valor del metro cuadro en venta",
    vs2: "Valor del metro cuadro en renta",
    rse: "Rezago social y económico"
  }

  onDateChanged(event: IMyDateModel, datepicker: number) {
    if (event.date == undefined)
      return;
    //datepicker inicial
    if (datepicker == 0) {
      this.optionsFechaFinal.disableUntil = event.date;
      this.initialDate = event.jsdate;// Date.parse(event.date.year + '-' + event.date.month + '-' + event.date.day) / 1000;

      this.optionsFechaFinal = {
        // other options...
        dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
        monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
        todayBtnTxt: "Hoy",
        firstDayOfWeek: "mo",
        sunHighlight: true,
        dateFormat: 'dd/mm/yyyy',
        disableUntil: event.date,
        selectionTxtFontSize: '13px',
        height: '25px'
      };

      //datepicker final
    } else if (datepicker == 1) {


      this.finalDate = event.jsdate;//Date.parse(event.date.year + '-' + event.date.month + '-' + event.date.day) / 1000;
      this.optionsFechaInicial = {
        // other options...
        dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
        monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
        todayBtnTxt: "Hoy",
        firstDayOfWeek: "mo",
        sunHighlight: true,
        dateFormat: 'dd/mm/yyyy',
        disableSince: event.date,
        selectionTxtFontSize: '13px',
        height: '25px'
      };

    }


  }

  private optionsFechaInicial: IMyOptions = {
    // other options...
    dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
    monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
    todayBtnTxt: "Hoy",
    firstDayOfWeek: "mo",
    sunHighlight: true,
    dateFormat: 'dd/mm/yyyy',
    selectionTxtFontSize: '13px',
    height: '25px'
  };

  private optionsFechaFinal: IMyOptions = {
    // other options...
    dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
    monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
    todayBtnTxt: "Hoy",
    firstDayOfWeek: "mo",
    sunHighlight: true,
    dateFormat: 'dd/mm/yyyy',
    selectionTxtFontSize: '13px',
    height: '25px'
  };

}