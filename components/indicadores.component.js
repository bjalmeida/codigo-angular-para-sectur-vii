"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var IndicadorComponent = (function () {
    function IndicadorComponent() {
        this.showHistorial = true;
        this.colSpan = 7;
        this.dataLine = [
            [65, 59, 80, 81, 56, 55, 40],
            [28, 48, 40, 19, 86, 27, 90]
        ];
        // lineChart
        this.lineChartData = [
            { data: [65, 59, 80, 81, 56, 55, 40], label: 'destino A' },
            { data: [28, 48, 40, 19, 86, 27, 90], label: 'pueblo B' },
            { data: [18, 48, 77, 9, 100, 27, 40], label: 'candidato C' }
        ];
        this.lineChartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
        this.lineChartOptions = {
            responsive: true
        };
        this.lineChartColors = [
            {
                backgroundColor: 'rgba(148,159,177,0.2)',
                borderColor: 'rgba(148,159,177,1)',
                pointBackgroundColor: 'rgba(148,159,177,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.8)'
            },
            {
                backgroundColor: 'rgba(77,83,96,0.2)',
                borderColor: 'rgba(77,83,96,1)',
                pointBackgroundColor: 'rgba(77,83,96,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(77,83,96,1)'
            },
            {
                backgroundColor: 'rgba(148,159,177,0.2)',
                borderColor: 'rgba(148,159,177,1)',
                pointBackgroundColor: 'rgba(148,159,177,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.8)'
            }
        ];
        this.lineChartLegend = true;
        this.lineChartType = 'line';
    }
    IndicadorComponent.prototype.toggle = function () {
        this.showHistorial = !this.showHistorial;
        if (this.showHistorial == true) {
            this.colSpan = 9;
        }
        else {
            this.colSpan = 7;
        }
    };
    IndicadorComponent.prototype.changeColor = function (val) {
        if (val > 70 && val <= 100) {
            return 'verdeBg';
        }
        else if (val >= 50 && val <= 70) {
            return 'naranjaBg';
        }
        else if (val >= 0 && val < 50) {
            return 'rojoBg';
        }
        else {
            return 'grisBg';
        }
    };
    IndicadorComponent.prototype.ngOnInit = function () {
        this.pueblos = [
            {
                nombre: 'pueblo',
                estado: 'mexico',
                tipo: 'pueblo magico'
            },
            {
                nombre: 'puebla',
                estado: 'lugar n',
                tipo: 'destino turistico'
            },
            {
                nombre: 'colima',
                estado: 'este',
                tipo: 'ciudad magica'
            }];
    };
    IndicadorComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'indicadores',
            templateUrl: '../templates/indicadores.component.html',
        }), 
        __metadata('design:paramtypes', [])
    ], IndicadorComponent);
    return IndicadorComponent;
}());
exports.IndicadorComponent = IndicadorComponent;
//# sourceMappingURL=indicadores.component.js.map