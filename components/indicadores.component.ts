import { Component, OnInit, ElementRef } from '@angular/core';
import { IndicadorService } from '../services/indicadores.service';
import { ConfigService } from '../services/config.service';
import { IMyOptions, IMyDateModel } from 'mydatepicker';
import { Response } from '@angular/http';
declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'indicadores',
  templateUrl: '../templates/indicadores.component.html',

})

export class IndicadorComponent implements OnInit {


  private optionsFechaInicial: IMyOptions = {
    // other options...
    dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
    monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
    todayBtnTxt: "Hoy",
    firstDayOfWeek: "mo",
    sunHighlight: true,
    dateFormat: 'dd/mm/yyyy',
    selectionTxtFontSize: '13px',
    height: '25px'
  };

  private optionsFechaFinal: IMyOptions = {
    // other options...
    dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
    monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
    todayBtnTxt: "Hoy",
    firstDayOfWeek: "mo",
    sunHighlight: true,
    dateFormat: 'dd/mm/yyyy',
    selectionTxtFontSize: '13px',
    height: '25px'
  };

  d = new Date();
  title = "extraccion_" + this.d.getDate + "-" + this.d.getMonth + "-" + this.d.getFullYear + ".xlsx";
  showHistorial: boolean = true;
  tipo_localidad = ['Pueblo Mágico', 'Destino prioritario', 'Candidato a Pueblo Mágico'];
  colSpan: number = 7;
  destinos: Object[];
  modoDatosCrudos: boolean = false; // usado para editar campos
  data: Object[] = [];
  tabSelected = ["mediaOferta", "mediaDemanda", "mediaSeguridad", "mediaAccesibilidad", "mediaComunidad", "mediaMercadotecnea", "general"];

  ordenarPor = { tab: "general", desc: [true, true, true, true, true, true, true] };
  tabOferta: string[] = [];
  tabDemanda: string[] = [];
  tabSeguridad: string[] = [];
  tabComunidad: string[] = [];
  tabAccess: string[] = [];
  tabMercadotecnia: string[] = [];
  initialDate: Date;
  finalDate: Date;
  filter: string;
  headers: string[] = [];
  localidades: string[] = [];
  graphs: Object[] = [];

  selectedDestinos: any[] = [];
  constructor(private indicadorService: IndicadorService, private configService: ConfigService, private elementRef: ElementRef) {


  }



  onDateChanged(event: IMyDateModel, datepicker: number) {
    if (event.date == undefined)
      return;
    //datepicker inicial
    if (datepicker == 0) {
      this.optionsFechaFinal.disableUntil = event.date;
      this.initialDate = event.jsdate;// Date.parse(event.date.year + '-' + event.date.month + '-' + event.date.day) / 1000;

      this.optionsFechaFinal = {
        // other options...
        dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
        monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
        todayBtnTxt: "Hoy",
        firstDayOfWeek: "mo",
        sunHighlight: true,
        dateFormat: 'dd/mm/yyyy',
        disableUntil: event.date,
        selectionTxtFontSize: '13px',
        height: '25px'
      };

      //datepicker final
    } else if (datepicker == 1) {


      this.finalDate = event.jsdate;//Date.parse(event.date.year + '-' + event.date.month + '-' + event.date.day) / 1000;
      this.optionsFechaInicial = {
        // other options...
        dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
        monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
        todayBtnTxt: "Hoy",
        firstDayOfWeek: "mo",
        sunHighlight: true,
        dateFormat: 'dd/mm/yyyy',
        disableSince: event.date,
        selectionTxtFontSize: '13px',
        height: '25px'
      };

    }


  }

  toggle(): void {



    $('#preloader').delay(0).fadeIn('fast');
    $('#status').fadeIn('fast');

    this.showHistorial = !this.showHistorial;
    if (this.showHistorial == true) {
      this.colSpan = 9;

    } else {
      this.colSpan = 7;

    }

    $('table.ind-style').floatThead('reflow');


    $('#status').fadeOut(550);
    $('#preloader').delay(550).fadeOut('slow');




  }


  setSubIndicador(subindicador, position) {
    let key: string;
    if (subindicador == "general")
      key = subindicador;
    else
      key = this.obtenKey(position, subindicador);


    if (this.selectedDestinos == undefined && key == undefined)
      return;

    this.indicadorService.getHistorico(key).then(data => {

      this.updateGraph(data, subindicador);


    });

  }

//BORRAR
  updateGraph(data, subindicador) {
   
  }
  //
//DESCOMENTAR
 /*
  var chartIndicadores; 
    IndicadorComponent.prototype.updateGraph = function (data, subindicador) {
        var arrayGraph = this.graphChart();
		chartIndicadores = AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "light",
    "legend": {
        "useGraphSettings": true
    },
    "dataProvider": data,
    "valueAxes": [{
        "integersOnly": true,
        "maximum": 10,
        "minimum": 0,
        "axisAlpha": 0,
        "dashLength": 5,
        "gridCount": 10,
        "position": "left",
        "title": "Valoracion en " + subindicador
    }],
   
    "graphs":  arrayGraph,
    "chartScrollbar": {
        "graph":"g1",
        "gridAlpha":0,
        "color":"#888888",
        "scrollbarHeight":55,
        "backgroundAlpha":0,
        "selectedBackgroundAlpha":0.1,
        "selectedBackgroundColor":"#888888",
        "graphFillAlpha":0,
        "autoGridCount":true,
        "selectedGraphFillAlpha":0,
        "graphLineAlpha":0.2,
        "graphLineColor":"#c2c2c2",
        "selectedGraphLineColor":"#888888",
        "selectedGraphLineAlpha":1

    },
    "chartCursor": {
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "fecha",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "fillAlpha": 0.05,
        "fillColor": "#000000",
        "gridAlpha": 0,
        "position": "top"
    },
    "export": {
    	"enabled": true,
        "position": "bottom-right"
     }
});

    };
    IndicadorComponent.prototype.graphChart = function () {
        var arrayGraph = [];
        this.selectedDestinos.forEach(function (destino) {
            var graph = {
                "balloonText": destino.charAt(0).toUpperCase() + destino.slice(1) + ": [[value]]",
                 "lineColor": randomColor(),
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": destino.charAt(0).toUpperCase() + destino.slice(1),
                "valueField": destino.toLowerCase(),
                "fillAlphas": 0
            };
            arrayGraph.push(graph);
        });
        return arrayGraph;
    };

*/
  getName(obj: any): string {


    return obj.toLowerCase();


  }


  changeColor(destino: Object, tab: string): string {

    if (this.modoDatosCrudos == true)
      tab = tab.substring(1, tab.length);


    var val = destino[tab];

    if (val == null || val == undefined || val == "NaN")
      return 'grisBg';


    if (val > 7.0 && val <= 10.0) { //verde
      return 'verdeBg';
    } else if (val >= 5.0 && val <= 7.0) { //naranja
      return 'naranjaBg';
    } else if (val >= 0.0 && val < 5.0) { //rojo      
      return 'rojoBg';
    }




  }




  ngOnInit(): void {

    $('table.ind-style').floatThead({
      position: 'fixed'
    });

    $('table.ind-style').floatThead('reflow');

    this.fetch();
    this.indicadorService.getIndicadoresCatalogo().then(data => {
      this.data = data;
      this.fillTabMenu();
      this.setHeader();
    });

    this.configService.getDestinos().then(data => {

      data.forEach(element => {
        var tmp;
        if (element.localidad == null || element.localidad == undefined)
          tmp = element.municipioNombre.toLowerCase();

        else
          tmp = element.localidad.toLowerCase();

        this.localidades.push(tmp.charAt(0).toUpperCase() + tmp.slice(1));
      });

    });


  }

  changeOrder(position: number) {
    this.ordenarPor.tab = this.tabSelected[position];
    this.ordenarPor.desc[position] = !this.ordenarPor.desc[position];

    if (this.ordenarPor.desc[position] == true)
      this.ordenarPor.tab = '-' + this.ordenarPor.tab;



  }

  fetch(): void {
    $('#preloader').delay(0).fadeIn('fast');
    $('#status').fadeIn('fast');
    this.destinos = [];
    this.indicadorService.getIndicadoresData().then(data => {
      this.destinos = data;
      $('#status').fadeOut(550);
      $('#preloader').delay(550).fadeOut('slow');
    });

  }

  sendQuery() {
    /*Agregar validaciones menores i.e. fechas no vacias! */
    $('#preloader').delay(0).fadeIn('fast');
    $('#status').fadeIn('fast');
    this.destinos = [];

    this.indicadorService.getIndicadoresDataFecha(this.initialDate.getTime() / 1000, this.finalDate.getTime() / 1000).then(data => {
      this.destinos = data;
      $('#status').fadeOut(550);
      $('#preloader').delay(550).fadeOut('slow');
    });

  }

  //cambia indicador desglosado, position es el lugar en el arreglo tabSelected[]
  setChange(tab: string, position: number): void {
    let tabKey: string;
    tabKey = this.obtenKey(position, tab);
    if (this.modoDatosCrudos == true)
      tabKey = 'd' + tabKey;


    this.tabSelected[position] = tabKey;
    this.setHeader();
  }

  obtenKey(position: number, tab: string) {


    switch (position) {

      case 0: {

        for (var key in this.oferta) {
          if (this.oferta.hasOwnProperty(key)) {

            if (this.oferta[key] === tab) {
              return key;

            }
          }
        }

        break;
      }

      case 1: {

        for (var key in this.demanda) {
          if (this.demanda.hasOwnProperty(key)) {
            if (this.demanda[key] === tab) {
              return key;
            }
          }
        }

        break;
      }

      case 2: {

        for (var key in this.seguridad) {
          if (this.seguridad.hasOwnProperty(key)) {
            if (this.seguridad[key] === tab) {
              return key;
            }
          }
        }

        break;
      }

      case 3: {
        for (var key in this.accesibilidad) {
          if (this.accesibilidad.hasOwnProperty(key)) {
            if (this.accesibilidad[key] === tab) {
              return key;
            }
          }
        }

        break;
      }
      case 4: {

        for (var key in this.comunidad) {
          if (this.comunidad.hasOwnProperty(key)) {
            if (this.comunidad[key] === tab) {
              return key;
            }
          }
        }

        break;
      }

      case 5: {

        for (var key in this.mercadotecnia) {
          if (this.mercadotecnia.hasOwnProperty(key)) {
            if (this.mercadotecnia[key] === tab) {
              return key;
            }
          }
        }

        break;
      }


    }
  }

  setCssColor() {
    if (this.modoDatosCrudos) {
      return "blancoBg";
    } else {
      return "grisBg2"; //whatever color
    }
  }

  modoDatos() {
    this.modoDatosCrudos = !this.modoDatosCrudos;

    if (this.modoDatosCrudos == true)
      for (var i = 0; i < this.tabSelected.length; i++)
        this.tabSelected[i] = 'd' + this.tabSelected[i];
    else
      for (var i = 0; i < this.tabSelected.length; i++)
        this.tabSelected[i] = this.tabSelected[i].substring(1, this.tabSelected[i].length);


  }


  ngAfterViewInit() {
    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = "./resources/js/app/jquery.base64.min.js";
    this.elementRef.nativeElement.appendChild(s);

  }


  activate() {
    if (this.modoDatosCrudos == true)
      return "btn btn-success  btn-sm active";
    else
      return "btn btn-success  btn-sm";
  }


  checkNaN(val: any) {
    if (val == "Infinity" || isNaN(val) || val === null || val === undefined) return false;
    else
      return true;

  }

  fillTabMenu() {


    this.tabOferta = [];
    this.tabDemanda = [];
    this.tabSeguridad = [];
    this.tabComunidad = [];
    this.tabAccess = [];
    this.tabMercadotecnia = [];


    for (var key in this.oferta) {
      if (this.oferta.hasOwnProperty(key)) {
        //    var value = this.oferta[key]; console.log(key + ":" + value);
        this.tabOferta.push(this.oferta[key]);
      }
    }

    for (var key in this.demanda) {
      if (this.demanda.hasOwnProperty(key)) {
        this.tabDemanda.push(this.demanda[key]);
      }
    }

    for (var key in this.seguridad) {
      if (this.seguridad.hasOwnProperty(key)) {
        this.tabSeguridad.push(this.seguridad[key]);
      }
    }

    for (var key in this.comunidad) {
      if (this.comunidad.hasOwnProperty(key)) {
        this.tabComunidad.push(this.comunidad[key]);
      }
    }

    for (var key in this.accesibilidad) {
      if (this.accesibilidad.hasOwnProperty(key)) {
        this.tabAccess.push(this.accesibilidad[key]);
      }
    }

    for (var key in this.mercadotecnia) {
      if (this.mercadotecnia.hasOwnProperty(key)) {
        this.tabMercadotecnia.push(this.mercadotecnia[key]);
      }
    }

  }

  prefijo(tab: string): string {

    let dinero = ["dph", "dpr", "dpt", "dvs1", "dvs2"];

    for (var i = 0; i < dinero.length; i++)
      if (tab === dinero[i])
        return '$'


    return '';
  }

  setHeader() {

    this.headers = [];
    this.headers.push("Destino");
    this.headers.push("");

    if (this.modoDatosCrudos == true)
      for (var i = 0; i < this.tabSelected.length; i++)
        this.tabSelected[i] = this.tabSelected[i].substring(1, this.tabSelected[i].length);

    this.headers.push(this.oferta[this.tabSelected[0]]);
    this.headers.push(this.demanda[this.tabSelected[1]]);
    this.headers.push(this.seguridad[this.tabSelected[2]]);
    this.headers.push(this.accesibilidad[this.tabSelected[3]]);
    this.headers.push(this.comunidad[this.tabSelected[4]]);
    this.headers.push(this.mercadotecnia[this.tabSelected[5]]);
    this.headers.push("Promedio");

    if (this.modoDatosCrudos == true)
      for (var i = 0; i < this.tabSelected.length; i++)
        this.tabSelected[i] = 'd' + this.tabSelected[i];


  }

  fnExcelReport() {

    var tab_text = "<table>";
    var textRange;
    var j = 0;
    var tab = document.getElementById('Indicadores'); // id of table
    var lines = this.destinos.length;
    var head = "<td bgcolor='#47d147' border='1px'>Destino</td> <td bgcolor='#47d147' border='1px'>Estado</td> <td bgcolor='#47d147' border='1px'>Tipo Destino</td>";
    var row = '';

    //encabezados
    for (var key in this.oferta)
      if (this.oferta.hasOwnProperty(key))
        head = head + "<td bgcolor='#47d147' border='1px'>" + this.oferta[key] + '</td>'


    for (var key in this.demanda)
      if (this.demanda.hasOwnProperty(key))
        head = head + "<td bgcolor='#47d147' border='1px'>" + this.demanda[key] + '</td>'


    for (var key in this.seguridad)
      if (this.seguridad.hasOwnProperty(key))
        head = head + "<td bgcolor='#47d147' border='1px'>" + this.seguridad[key] + '</td>'


    for (var key in this.comunidad)
      if (this.comunidad.hasOwnProperty(key))
        head = head + "<td bgcolor='#47d147' border='1px'>" + this.comunidad[key] + '</td>'


    for (var key in this.accesibilidad)
      if (this.accesibilidad.hasOwnProperty(key))
        head = head + "<td bgcolor='#47d147' border='1px'>" + this.accesibilidad[key] + '</td>'


    for (var key in this.mercadotecnia)
      if (this.mercadotecnia.hasOwnProperty(key))
        head = head + "<td bgcolor='#47d147' border='1px'>" + this.mercadotecnia[key] + '</td>'


    head = head + "<td bgcolor='#47d147' border='1px'>Promedio</td>"


    if (lines > 0) {

      tab_text = tab_text + "<tr>" + head + '</tr>';
    }
    //Cuerpo tabla
    // table destinos lines, loop starting from 1
    for (j = 0; j < lines; j++) {
      row = '';
      row = row + "<td>" + this.transform(this.destinos[j]["destino"]) + "</td>";
      row = row + "<td>" + this.destinos[j]["estado"] + "</td>";
      row = row + "<td>" + this.tipo_localidad[this.destinos[j]["tipoDestino"] - 1] + "</td>";


      for (var key in this.oferta)
        if (this.oferta.hasOwnProperty(key))
          if (this.destinos[j][key] != null && this.destinos[j][key] != undefined && this.destinos[j][key] != "NaN")
            row = row + "<td border='1px' text-align='center'>" + (Math.floor(this.destinos[j][key] * 100) / 100) + '</td>'
          else
            row = row + "<td border='1px' text-align='center'>" + 'N/A' + '</td>'


      for (var key in this.demanda)
        if (this.demanda.hasOwnProperty(key))
          if (this.destinos[j][key] != null && this.destinos[j][key] != undefined && this.destinos[j][key] != "NaN")
            row = row + "<td border='1px' text-align='center'>" + (Math.floor(this.destinos[j][key] * 100) / 100) + '</td>'
          else
            row = row + "<td border='1px' text-align='center'>" + 'N/A' + '</td>'

      for (var key in this.seguridad)
        if (this.seguridad.hasOwnProperty(key))
          if (this.destinos[j][key] != null && this.destinos[j][key] != undefined && this.destinos[j][key] != "NaN")
            row = row + "<td border='1px' text-align='center'>" + (Math.floor(this.destinos[j][key] * 100) / 100) + '</td>'
          else
            row = row + "<td border='1px' text-align='center'>" + 'N/A' + '</td>'

      for (var key in this.comunidad)
        if (this.comunidad.hasOwnProperty(key))
          if (this.destinos[j][key] != null && this.destinos[j][key] != undefined && this.destinos[j][key] != "NaN")
            row = row + "<td border='1px' text-align='center'>" + (Math.floor(this.destinos[j][key] * 100) / 100) + '</td>'
          else
            row = row + "<td border='1px' text-align='center'>" + 'N/A' + '</td>'

      for (var key in this.accesibilidad)
        if (this.accesibilidad.hasOwnProperty(key))
          if (this.destinos[j][key] != null && this.destinos[j][key] != undefined && this.destinos[j][key] != "NaN")
            row = row + "<td border='1px' text-align='center'>" + (Math.floor(this.destinos[j][key] * 100) / 100) + '</td>'
          else
            row = row + "<td border='1px' text-align='center'>" + 'N/A' + '</td>'

      for (var key in this.mercadotecnia)
        if (this.mercadotecnia.hasOwnProperty(key))
          if (this.destinos[j][key] != null && this.destinos[j][key] != undefined && this.destinos[j][key] != "NaN")
            row = row + "<td border='1px' text-align='center'>" + (Math.floor(this.destinos[j][key] * 100) / 100) + '</td>'
          else
            row = row + "<td border='1px' text-align='center'>" + 'N/A' + '</td>'

      row = row + "<td text-align='center'>" + (Math.floor(this.destinos[j]["general"] * 100) / 100) + "</td>";

      if ((j % 2) == 0) {

        row = row.replace(/<td[^>]*>/g, "<td bgcolor='#ededed' text-align='center' border='1px solid black'>");

      }


      tab_text = tab_text + "<tr>" + row + "</tr>";

    }

    tab_text = tab_text + "</table>";

    // console.log(tab_text); // aktivate so see the result (press F12 in browser)

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    var sa = window.open('data:application/vnd.ms-excel;base64,' + $.base64.encode(tab_text));

    return sa;

  }


  refresh() {
    this.fetch(); this.fillTabMenu(); this.modoDatosCrudos = false;
    this.tabSelected = ["mediaOferta", "mediaDemanda", "mediaSeguridad", "mediaAccesibilidad", "mediaComunidad", "mediaMercadotecnea", "general"];
    this.setHeader();
    this.ordenarPor = { tab: "general", desc: [true, true, true, true, true, true, true] };
  }

  transform(value: any) {
    if (value)
      return value.charAt(0).toUpperCase() + value.slice(1);
  }


  setIcon(key: string) {

    if (this.modoDatosCrudos == true)
      key = key.substring(1, key.length);


    return "./resources/img/iconos/" + key + ".png";
  }

  setTooltip(tab: string, origin: number) {
    if (this.modoDatosCrudos == true) {
      tab = tab.substring(1, tab.length);
    }
    switch (origin) {

      case 0: {

        for (var key in this.oferta) {
          if (this.oferta.hasOwnProperty(key)) {

            if (key === tab)
              return this.oferta[key];


          }
        }

        break;
      }

      case 1: {

        for (var key in this.demanda) {
          if (this.demanda.hasOwnProperty(key)) {

            if (key === tab)
              return this.demanda[key];
          }
        }

        break;
      }

      case 2: {

        for (var key in this.seguridad) {
          if (this.seguridad.hasOwnProperty(key)) {
            if (key === tab)
              return this.seguridad[key];
          }
        }

        break;
      }

      case 3: {
        for (var key in this.accesibilidad) {
          if (this.accesibilidad.hasOwnProperty(key)) {
            if (key === tab)
              return this.accesibilidad[key];
          }
        }

        break;
      }
      case 4: {

        for (var key in this.comunidad) {
          if (this.comunidad.hasOwnProperty(key)) {
            if (key === tab)
              return this.comunidad[key];
          }
        }

        break;
      }

      case 5: {

        for (var key in this.mercadotecnia) {
          if (this.mercadotecnia.hasOwnProperty(key)) {
            if (key === tab)
              return this.mercadotecnia[key];
          }
        }

        break;
      }

    }


  }


  oferta = {
    mediaOferta: "Promedio General Oferta",
    cia: "Calidad de la infraestructura de acceso",
    ca: "Calidad de los atractivos",
    ca1: "Calidad de alojamiento",
    ca2: "Calidad de alimentos",
    esd: "Estado de los señalamientos para llegar al destino",
    egct: "Estado general de la calidad del transporte",
    feh: "Facilidad de encontrar el hospedaje",
    fea: "Facilidad de encontrar hospedaje (alternativo)",
    rcp: "Relación calidad precio",
    va: "Variedad de los atractivos",
    ph: "Precio Hoteles",
    pr: "Precio Restaurantes",
    pt: "Precio Transporte"
  }


  demanda = {
    mediaDemanda: "Promedio General Demanda",
    cat: "Relevancia de la atractividad",

    ep: "Estadía promedio"
  }


  seguridad = {
    mediaSeguridad: "Promedio General Seguridad",
    ps: "Percepción de seguridad",
    rn: "Riesgos naturales"
  }

  mercadotecnia = {
    mediaMercadotecnea: "Promedio General Mercadotécnia",
    ed: "Experiencia diferente",
    dit: "Disponibilidad de información turística",
    ev1: "Experiencia de viaje",
    ev2: "Emoción al visitar",
    pp: "Percepción de la promoción"
  }

  accesibilidad = {
    mediaAccesibilidad: "Promedio General Accesibilidad",
    ci: "Canales de información",
    cc: "Canales de comunicación",
    st: "Sistema de transporte",
    ad: "Accesibilidad 'discapacidad'",
    ef: "Entorno físico"
  }

  comunidad = {
    mediaComunidad: "Promedio General Comunidad",
    e: "Empleo",
    h: "Hospitalidad",
    vs1: "Valor del metro cuadro en venta",
    vs2: "Valor del metro cuadro en renta",
    rse: "Rezago social y económico"
  }

}

