import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { IndicadorService } from '../services/indicadores.service';
import { Response } from '@angular/http';
import 'chart.js/src/chart';
declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'dashboard',
  templateUrl: '../templates/inicio.component.html',
})


export class InicioComponent implements OnInit {

  BestLocations: Object = null;
  tipo_localidad = ['Pueblo Mágico', 'Destino prioritario', 'Candidato a Pueblo Mágico'];
  dataCharts: Object[] = [];
  tab = ["mediaOferta", "mediaDemanda", "mediaSeguridad", "mediaComunidad", "mediaMercadotecnea", "mediaAccesibilidad"]
  encabezadosGraficas = ["Mejor Oferta", "Mejor Demanda", "Mejor Seguridad", "Mejor Comunidad", "Mejor Mercadotécnia", "Mejor Accesibilidad"];
  valoracionesDestino: Object[] = [];
  dataLineCharts: Object[] = [];
  historicoContainer: Object = null;
  lista_destino: string[] = [];
  static offsetGraficas: number = 0;
  show_historico = false;
  selected: number;
  carrusel: number = 0;


  options = {
    pointLabelFontSize: 20,
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    tooltips: {
      enabled: true,
      callbacks: {
        label: function (tooltipItem, data) {

          // 
          var dataset = data.datasets[tooltipItem.datasetIndex];
          var label = data.labels[tooltipItem.index];
          var indicador;

          var tabs = {
            mediaOferta: "Promedio General Oferta",
            cia: "Calidad de la infraestructura de acceso",
            ca: "Calidad de los atractivos",
            ca1: "Calidad de alojamiento",
            ca2: "Calidad de alimentos",
            esd: "Estado de los señalamientos para llegar al destino",
            egct: "Estado general de la calidad del transporte",
            feh: "Facilidad de encontrar el hospedaje",
            fea: "Facilidad de encontrar hospedaje (alternativo)",
            rcp: "Relación calidad precio",
            va: "Variedad de los atractivos",
            ph: "Precio Hoteles",
            pr: "Precio Restaurantes",
            pt: "Precio Transporte",
            mediaDemanda: "Promedio General Demanda",
            cat: "Relevancia de la atractividad",
            ata: "Porcentaje de visitantes extranjeros",
            ep: "Estadía promedio",
            mediaSeguridad: "Promedio General Seguridad",
            ps: "Percepción de seguridad",
            rn: "Riesgos naturales",
            mediaMercadotecnea: "Promedio General Mercadotécnia",
            ed: "Experiencia diferente",
            dit: "Disponibilidad de información turística",
            ev1: "Experiencia de viaje",
            ev2: "Emoción al visitar",
            pp: "Percepción de la promoción",
            mediaAccesibilidad: "Promedio General Accesibilidad",
            ci: "Canales de información",
            cc: "Canales de comunicación",
            st: "Sistema de transporte",
            ad: "Accesibilidad 'discapacidad'",
            ef: "Entorno físico",
            mediaComunidad: "Promedio General Comunidad",
            e: "Empleo",
            h: "Hospitalidad",
            vs1: "Valor del metro cuadro en venta",
            vs2: "Valor del metro cuadro en renta",
            rse: "Rezago social y económico"
          }


          for (var key in tabs) {

            if (tabs.hasOwnProperty(key)) {

              if (key == label) {

                indicador = tabs[key];
                break;
              }
            }
          }

          if (indicador != undefined)
            return indicador + " : " + dataset.data[tooltipItem.index];
          else

            return "" + (Math.floor(dataset.data[tooltipItem.index] * 100) / 100);
        }
      }
    }
  };


  optionsLine = {
    pointLabelFontSize: 20,
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: true
    },
    scales: {
      xAxes: [{
        display: true
      }]
    },
    tooltips: {
      mode: 'label'
    }
  };

  optionsBar = {
    onClick: function (evt, item) {
      if (item[0] != undefined)
        InicioComponent.offsetGraficas = item[0]._index;
    },
    //this.graphClickEvent,
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: true
      }]
    },
    legend: {
      display: false
    },
    tooltips: {
      enabled: true
    }
  };



  constructor(private indicadorService: IndicadorService, private elementRef: ElementRef) {

  }


  ngAfterViewInit() {

  }


  ngOnInit(): void {

    this.BestLocations = null;

    this.dataCharts = [];
    var destinosLabels = [];
    var datos = [];
    this.valoracionesDestino = [];
    //   $('#preloader').delay(0).fadeIn('fast');
    //   $('#status').fadeIn('fast');

    this.indicadorService.gettodayBest().then(data => {
      this.BestLocations = data[0];
      this.valoracionesDestino = data.slice(1);

      for (var i = 0; i < this.tab.length; i++) {
        if (this.BestLocations[this.tab[i]] != undefined || this.BestLocations[this.tab[i]] != null || this.BestLocations[this.tab[i]] != "NaN")
          datos.push(Math.floor(this.BestLocations[this.tab[i]] * 100) / 100);
      }

      data = {
        labels: ["Oferta", "Demanda", "Seguridad", "Comunidad", "Mercadotécnia", "Accesibilidad"],
        datasets: [
          {

            data: datos,
            backgroundColor: [
              "rgba(50, 181, 236, 0.3)"
            ],

          }
        ]
      };

      this.dataCharts.push(data);


      var indexIndicador = 0;

      for (var j = 0; j < 6; j++) { //categorias
        destinosLabels = [];
        datos = [];
        for (var i = 0; i < 10; i++) { //locaciones

          var pos = i + (j * 10);
          destinosLabels.push(this.mayusPrimerLetra(this.valoracionesDestino[pos]["destino"]));

          datos.push(Math.floor(this.valoracionesDestino[pos][this.tab[indexIndicador]] * 100) / 100);
        }


        indexIndicador++;

        data = {
          labels: destinosLabels,
          datasets: [
            {

              data: datos,
              backgroundColor: [
                "rgba(0, 138, 213, 0.9)",
                "rgba(0, 138, 213, 0.9)",
                "rgba(0, 138, 213, 0.8)",
                "rgba(0, 138, 213, 0.8)",
                "rgba(0, 138, 213, 0.7)",
                "rgba(0, 138, 213, 0.7)",
                "rgba(0, 138, 213, 0.6)",
                "rgba(0, 138, 213, 0.6)",
                "rgba(0, 138, 213, 0.5)",
                "rgba(0, 138, 213, 0.5)"
              ],

            }
          ]
        };
        this.dataCharts.push(data);
      }

    });

    this.indicadorService.getMonthBest().then(data => {

      this.historicoContainer = data;

    });

    $('#status').fadeOut(550);
    $('#preloader').delay(550).fadeOut('slow');

  }


  getPos() {

    return InicioComponent.offsetGraficas;
  }
  //carrusel graficas historico
  anterior() {
    if (InicioComponent.offsetGraficas <= 0)
      InicioComponent.offsetGraficas = 0;
    else
      InicioComponent.offsetGraficas--;
  }

  siguiente() {

    if (InicioComponent.offsetGraficas >= 9)
      InicioComponent.offsetGraficas = 9;
    else
      InicioComponent.offsetGraficas++;


  }

  //carrusel mejores destinos por categorias

  anteriorCategoria() {
    if (this.carrusel <= 0)
      this.carrusel = 0;
    else
      this.carrusel -= 2;
  }

  siguienteCategoria() {
    if (this.carrusel >= 4)
      this.carrusel = 4;
    else
      this.carrusel += 2;


  }





  changeHistorico(n: number) {


    this.show_historico = true;

    var label = [];
    //datos por indicador
    var dataline = [];
    var series = [];
    let indiceCharts = 0;//lleva conteo de todos las series de datos de cada destino
    this.lista_destino = [];
    this.dataLineCharts = [];

    switch (n) {
      case 0: {
        var destino = '';

        //etiquetas de cada indicador + tamaño de las series de datos en la grafica
        for (var key in this.oferta) {
          if (this.oferta.hasOwnProperty(key)) {
            series.push(this.oferta[key]);
            dataline[indiceCharts] = [];

            indiceCharts++;

          }
        }

        var ultimoIndice = 0;//lleva el conteo de hasta donde se encontro todos los destinos del mismo nombre

        for (var i = 0; i < this.historicoContainer["ofertaHistorico"].length; i++) {



          if (destino == '')
            destino = this.historicoContainer["ofertaHistorico"][i]["destino"];

          if (destino != this.historicoContainer["ofertaHistorico"][i]["destino"] || i == this.historicoContainer["ofertaHistorico"].length - 1) {

            this.lista_destino.push(destino);

            destino = this.historicoContainer["ofertaHistorico"][i]["destino"];


            for (var j = ultimoIndice; j < i; j++) {

              indiceCharts = 0;

              for (var key in this.oferta) {

                if (this.oferta.hasOwnProperty(key)) {

                  dataline[indiceCharts].push(Math.floor(this.historicoContainer["ofertaHistorico"][j][key] * 100) / 100);
                  indiceCharts++;

                }
              }
            }


            indiceCharts = 0;
            var dataset = [];

            for (indiceCharts = 0; indiceCharts < dataline.length; indiceCharts++) {


              var color = '#' + Math.random().toString(16).substr(-6);
              var set = {
                label: series[indiceCharts],
                fill: false,
                data: dataline[indiceCharts],
                pointColor: color, strokeColor: color,
                backgroundColor: color,
                borderColor: color,
                borderWidth: 0.5
              };

              dataset.push(set);
              dataline[indiceCharts] = [];


            }

            var grafica = {
              labels: label,
              datasets: dataset
            };


            this.dataLineCharts.push(grafica);

            label = [];
            ultimoIndice = i;

          }
          else {
            var date = new Date(this.historicoContainer["ofertaHistorico"][i]["fecha"]);
            label.push(date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());

          }

        }

        break;
      }
      case 1: {

        var destino = '';

        //etiquetas de cada indicador + tamaño de las series de datos en la grafica
        for (var key in this.demanda) {
          if (this.demanda.hasOwnProperty(key)) {
            series.push(this.demanda[key]);
            dataline[indiceCharts] = [];

            indiceCharts++;

          }
        }

        var ultimoIndice = 0;//lleva el conteo de hasta donde se encontro todos los destinos del mismo nombre

        for (var i = 0; i < this.historicoContainer["demandaHistorico"].length; i++) {

          if (destino == '')
            destino = this.historicoContainer["demandaHistorico"][i]["destino"];

          if (destino != this.historicoContainer["demandaHistorico"][i]["destino"] || i == this.historicoContainer["demandaHistorico"].length - 1) {

            this.lista_destino.push(destino);
            destino = this.historicoContainer["demandaHistorico"][i]["destino"];



            for (var j = ultimoIndice; j < i; j++) {

              indiceCharts = 0;

              for (var key in this.demanda) {

                if (this.demanda.hasOwnProperty(key)) {

                  dataline[indiceCharts].push(Math.floor(this.historicoContainer["demandaHistorico"][j][key] * 100) / 100);
                  indiceCharts++;

                }
              }
            }


            indiceCharts = 0;
            var dataset = [];

            for (indiceCharts = 0; indiceCharts < dataline.length; indiceCharts++) {


              var color = '#' + Math.random().toString(16).substr(-6);
              var set = {
                label: series[indiceCharts],
                fill: false,
                data: dataline[indiceCharts],
                pointColor: color, strokeColor: color,
                backgroundColor: color,
                borderColor: color,

                borderWidth: 0.5
              };

              dataset.push(set);
              dataline[indiceCharts] = [];


            }

            var grafica = {
              labels: label,
              datasets: dataset
            };


            this.dataLineCharts.push(grafica);

            label = [];
            ultimoIndice = i;

          }
          else {
            var date = new Date(this.historicoContainer["demandaHistorico"][i]["fecha"]);

            label.push(date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());

          }

        }

        break;
      }

      case 2: {

        var destino = '';

        //etiquetas de cada indicador + tamaño de las series de datos en la grafica
        for (var key in this.seguridad) {
          if (this.seguridad.hasOwnProperty(key)) {
            series.push(this.seguridad[key]);
            dataline[indiceCharts] = [];

            indiceCharts++;

          }
        }

        var ultimoIndice = 0;//lleva el conteo de hasta donde se encontro todos los destinos del mismo nombre

        for (var i = 0; i < this.historicoContainer["seguridadHistorico"].length; i++) {

          if (destino == '')
            destino = this.historicoContainer["seguridadHistorico"][i]["destino"];

          if (destino != this.historicoContainer["seguridadHistorico"][i]["destino"] || i == this.historicoContainer["seguridadHistorico"].length - 1) {

            this.lista_destino.push(destino);
            destino = this.historicoContainer["seguridadHistorico"][i]["destino"];



            for (var j = ultimoIndice; j < i; j++) {

              indiceCharts = 0;

              for (var key in this.seguridad) {

                if (this.seguridad.hasOwnProperty(key)) {

                  dataline[indiceCharts].push(Math.floor(this.historicoContainer["seguridadHistorico"][j][key] * 100) / 100);
                  indiceCharts++;

                }
              }
            }


            indiceCharts = 0;
            var dataset = [];

            for (indiceCharts = 0; indiceCharts < dataline.length; indiceCharts++) {


              var color = '#' + Math.random().toString(16).substr(-6);
              var set = {
                label: series[indiceCharts],
                fill: false,
                data: dataline[indiceCharts],
                pointColor: color, strokeColor: color,
                backgroundColor: color,
                borderColor: color,
                borderWidth: 0.5
              };

              dataset.push(set);
              dataline[indiceCharts] = [];


            }

            var grafica = {
              labels: label,
              datasets: dataset
            };


            this.dataLineCharts.push(grafica);

            label = [];
            ultimoIndice = i;

          }
          else {
            var date = new Date(this.historicoContainer["seguridadHistorico"][i]["fecha"]);

            label.push(date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());

          }

        }


        break;
      }
      case 3: {

        var destino = '';

        //etiquetas de cada indicador + tamaño de las series de datos en la grafica
        for (var key in this.comunidad) {
          if (this.comunidad.hasOwnProperty(key)) {
            series.push(this.comunidad[key]);
            dataline[indiceCharts] = [];

            indiceCharts++;

          }
        }

        var ultimoIndice = 0;//lleva el conteo de hasta donde se encontro todos los destinos del mismo nombre

        for (var i = 0; i < this.historicoContainer["comunidadHistorico"].length; i++) {

          if (destino == '')
            destino = this.historicoContainer["comunidadHistorico"][i]["destino"];

          if (destino != this.historicoContainer["comunidadHistorico"][i]["destino"] || i == this.historicoContainer["comunidadHistorico"].length - 1) {

            this.lista_destino.push(destino);

            destino = this.historicoContainer["comunidadHistorico"][i]["destino"];


            for (var j = ultimoIndice; j < i; j++) {

              indiceCharts = 0;

              for (var key in this.comunidad) {

                if (this.comunidad.hasOwnProperty(key)) {

                  dataline[indiceCharts].push(Math.floor(this.historicoContainer["comunidadHistorico"][j][key] * 100) / 100);
                  indiceCharts++;

                }
              }
            }


            indiceCharts = 0;
            var dataset = [];

            for (indiceCharts = 0; indiceCharts < dataline.length; indiceCharts++) {


              var color = '#' + Math.random().toString(16).substr(-6);
              var set = {
                label: series[indiceCharts],
                fill: false,
                data: dataline[indiceCharts],
                pointColor: color, strokeColor: color,
                backgroundColor: color,
                borderColor: color,

                borderWidth: 0.5
              };

              dataset.push(set);
              dataline[indiceCharts] = [];


            }

            var grafica = {
              labels: label,
              datasets: dataset
            };


            this.dataLineCharts.push(grafica);

            label = [];
            ultimoIndice = i;

          }
          else {
            var date = new Date(this.historicoContainer["comunidadHistorico"][i]["fecha"]);

            label.push(date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());

          }

        }

        break;
      }

      case 4: {

        var destino = '';

        //etiquetas de cada indicador + tamaño de las series de datos en la grafica
        for (var key in this.mercadotecnia) {
          if (this.mercadotecnia.hasOwnProperty(key)) {
            series.push(this.mercadotecnia[key]);
            dataline[indiceCharts] = [];

            indiceCharts++;

          }
        }

        var ultimoIndice = 0;//lleva el conteo de hasta donde se encontro todos los destinos del mismo nombre

        for (var i = 0; i < this.historicoContainer["mercadotecniaHistorico"].length; i++) {

          if (destino == '')
            destino = this.historicoContainer["mercadotecniaHistorico"][i]["destino"];

          if (destino != this.historicoContainer["mercadotecniaHistorico"][i]["destino"] || i == this.historicoContainer["mercadotecniaHistorico"].length - 1) {

            this.lista_destino.push(destino);
            destino = this.historicoContainer["mercadotecniaHistorico"][i]["destino"];



            for (var j = ultimoIndice; j < i; j++) {

              indiceCharts = 0;

              for (var key in this.mercadotecnia) {

                if (this.mercadotecnia.hasOwnProperty(key)) {

                  dataline[indiceCharts].push(Math.floor(this.historicoContainer["mercadotecniaHistorico"][j][key] * 100) / 100);
                  indiceCharts++;

                }
              }
            }


            indiceCharts = 0;
            var dataset = [];

            for (indiceCharts = 0; indiceCharts < dataline.length; indiceCharts++) {


              var color = '#' + Math.random().toString(16).substr(-6);
              var set = {
                label: series[indiceCharts],
                fill: false,
                data: dataline[indiceCharts],
                pointColor: color, strokeColor: color,
                backgroundColor: color,
                borderColor: color,
                borderWidth: 0.5
              };

              dataset.push(set);
              dataline[indiceCharts] = [];


            }

            var grafica = {
              labels: label,
              datasets: dataset
            };


            this.dataLineCharts.push(grafica);

            label = [];
            ultimoIndice = i;

          }
          else {
            var date = new Date(this.historicoContainer["mercadotecniaHistorico"][i]["fecha"]);

            label.push(date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());

          }

        }


        break;
      }

      case 5: {

        var destino = '';

        //etiquetas de cada indicador + tamaño de las series de datos en la grafica
        for (var key in this.accesibilidad) {
          if (this.accesibilidad.hasOwnProperty(key)) {
            series.push(this.accesibilidad[key]);
            dataline[indiceCharts] = [];

            indiceCharts++;

          }
        }

        var ultimoIndice = 0;//lleva el conteo de hasta donde se encontro todos los destinos del mismo nombre

        for (var i = 0; i < this.historicoContainer["accesibilidadHistorico"].length; i++) {

          if (destino == '')
            destino = this.historicoContainer["accesibilidadHistorico"][i]["destino"];

          if (destino != this.historicoContainer["accesibilidadHistorico"][i]["destino"] || i == this.historicoContainer["accesibilidadHistorico"].length - 1) {


            this.lista_destino.push(destino);
            destino = this.historicoContainer["accesibilidadHistorico"][i]["destino"];



            for (var j = ultimoIndice; j < i; j++) {

              indiceCharts = 0;

              for (var key in this.accesibilidad) {

                if (this.accesibilidad.hasOwnProperty(key)) {

                  dataline[indiceCharts].push(Math.floor(this.historicoContainer["accesibilidadHistorico"][j][key] * 100) / 100);
                  indiceCharts++;

                }
              }
            }


            indiceCharts = 0;
            var dataset = [];

            for (indiceCharts = 0; indiceCharts < dataline.length; indiceCharts++) {


              var color = '#' + Math.random().toString(16).substr(-6);
              var set = {
                label: series[indiceCharts],
                fill: false,
                data: dataline[indiceCharts],
                pointColor: color, strokeColor: color,
                backgroundColor: color,
                borderColor: color,
                borderWidth: 0.5
              };

              dataset.push(set);
              dataline[indiceCharts] = [];


            }

            var grafica = {
              labels: label,
              datasets: dataset
            };


            this.dataLineCharts.push(grafica);

            label = [];
            ultimoIndice = i;

          }
          else {
            var date = new Date(this.historicoContainer["accesibilidadHistorico"][i]["fecha"]);

            label.push(date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());

          }

        }


        break;
      }
    }


    setTimeout(function () {
      window.scrollTo(0, document.body.scrollHeight);
    }, 320);




  }


  setBestLocationOferta() {

    var datos = [];
    var label = [];


    for (var key in this.oferta)
      if (this.oferta.hasOwnProperty(key))
        if (isNaN(this.BestLocations[key]) || this.BestLocations[key] === null || this.BestLocations[key] === undefined)
          datos.push(0.0);
        else
          datos.push((Math.floor(this.BestLocations[key] * 100) / 100));



    for (var key in this.oferta) {
      if (this.oferta.hasOwnProperty(key)) {
        label.push(key);//this.oferta[key]
      }
    }


    var data = {
      labels: label,
      datasets: [
        {

          data: datos,
          backgroundColor: [
            "rgba(124, 181, 236, 0.3)"

          ],

        }
      ]
    };

    this.dataCharts[0] = data;

  }

  setBestLocationDemanda() {

    var datos = [];
    var label = [];


    for (var key in this.demanda)
      if (this.demanda.hasOwnProperty(key))
        if (isNaN(this.BestLocations[key]) || this.BestLocations[key] === null || this.BestLocations[key] === undefined)
          datos.push(0.0);
        else
          datos.push((Math.floor(this.BestLocations[key] * 100) / 100));

    for (var key in this.demanda) {
      if (this.demanda.hasOwnProperty(key)) {
        label.push(key);
      }
    }


    var data = {
      labels: label,
      datasets: [
        {

          data: datos,
          backgroundColor: [
            "rgba(124, 181, 236, 0.3)"

          ],

        }
      ]
    };

    this.dataCharts[0] = data;

  }


  setBestLocationSeguridad() {

    var datos = [];
    var label = [];


    for (var key in this.seguridad)
      if (this.seguridad.hasOwnProperty(key))
        if (isNaN(this.BestLocations[key]) || this.BestLocations[key] === null || this.BestLocations[key] === undefined)
          datos.push(0.0);
        else
          datos.push((Math.floor(this.BestLocations[key] * 100) / 100));

    for (var key in this.seguridad) {
      if (this.seguridad.hasOwnProperty(key)) {
        label.push(key);
      }
    }


    var data = {
      labels: label,
      datasets: [
        {

          data: datos,
          backgroundColor: [
            "rgba(124, 181, 236, 0.3)"
          ],

        }
      ]
    };

    this.dataCharts[0] = data;

  }


  setBestLocationComunidad() {

    var datos = [];
    var label = [];


    for (var key in this.comunidad)
      if (this.comunidad.hasOwnProperty(key))
        if (isNaN(this.BestLocations[key]) || this.BestLocations[key] === null || this.BestLocations[key] === undefined)
          datos.push(0.0);
        else
          datos.push((Math.floor(this.BestLocations[key] * 100) / 100));

    for (var key in this.comunidad) {
      if (this.comunidad.hasOwnProperty(key)) {
        label.push(key);
      }
    }


    var data = {
      labels: label,
      datasets: [
        {

          data: datos,
          backgroundColor: [
            "rgba(124, 181, 236, 0.3)"
          ],

        }
      ]
    };

    this.dataCharts[0] = data;

  }



  setBestLocationMercadotecnia() {

    var datos = [];
    var label = [];


    for (var key in this.mercadotecnia)
      if (this.mercadotecnia.hasOwnProperty(key))
        if (isNaN(this.BestLocations[key]) || this.BestLocations[key] === null || this.BestLocations[key] === undefined)
          datos.push(0.0);
        else
          datos.push((Math.floor(this.BestLocations[key] * 100) / 100));

    for (var key in this.mercadotecnia) {
      if (this.mercadotecnia.hasOwnProperty(key)) {
        label.push(key);
      }
    }


    var data = {
      labels: label,
      datasets: [
        {

          data: datos,
          backgroundColor: [
            "rgba(124, 181, 236, 0.3)"
          ],

        }
      ]
    };

    this.dataCharts[0] = data;

  }


  setBestLocationAccesibilidad() {

    var datos = [];
    var label = [];


    for (var key in this.accesibilidad)
      if (this.accesibilidad.hasOwnProperty(key))
        if (isNaN(this.BestLocations[key]) || this.BestLocations[key] === null || this.BestLocations[key] === undefined)
          datos.push(0.0);
        else
          datos.push((Math.floor(this.BestLocations[key] * 100) / 100));

    for (var key in this.accesibilidad) {
      if (this.accesibilidad.hasOwnProperty(key)) {
        label.push(key);
      }
    }


    var data = {
      labels: label,
      datasets: [
        {

          data: datos,
          backgroundColor: [
            "rgba(124, 181, 236, 0.3)"
          ],

        }
      ]
    };

    this.dataCharts[0] = data;

  }


  setBestDefault() {

    var data = {
      labels: ["Oferta", "Demanda", "Seguridad", "Comunidad", "Mercadotécnia", "Accesibilidad"],
      datasets: [
        {

          data: [this.BestLocations["mediaOferta"], this.BestLocations["mediaDemanda"], this.BestLocations["mediaSeguridad"],
          this.BestLocations["mediaComunidad"], this.BestLocations["mediaMercadotecnea"], this.BestLocations["mediaAccesibilidad"]],
          backgroundColor: [
            "rgba(124, 181, 236, 0.3)"
          ],

        }
      ]
    };
    this.dataCharts[0] = data;

  }

  oferta = {
    mediaOferta: "Promedio General Oferta",
    cia: "Calidad de la infraestructura de acceso",
    ca: "Calidad de los atractivos",
    ca1: "Calidad de alojamiento",
    ca2: "Calidad de alimentos",
    esd: "Estado de los señalamientos para llegar al destino",
    egct: "Estado general de la calidad del transporte",
    feh: "Facilidad de encontrar el hospedaje",
    fea: "Facilidad de encontrar hospedaje (alternativo)",
    rcp: "Relación calidad precio",
    va: "Variedad de los atractivos",
    ph: "Precio Hoteles",
    pr: "Precio Restaurantes",
    pt: "Precio Transporte"
  }


  demanda = {
    mediaDemanda: "Promedio General Demanda",
    cat: "Relevancia de la atractividad",

    ep: "Estadía promedio"
  }


  seguridad = {
    mediaSeguridad: "Promedio General Seguridad",
    ps: "Percepción de seguridad",
    rn: "Riesgos naturales"
  }

  mercadotecnia = {
    mediaMercadotecnea: "Promedio General Mercadotécnia",
    ed: "Experiencia diferente",
    dit: "Disponibilidad de información turística",
    ev1: "Experiencia de viaje",
    ev2: "Emoción al visitar",
    pp: "Percepción de la promoción"
  }

  accesibilidad = {
    mediaAccesibilidad: "Promedio General Accesibilidad",
    ci: "Canales de información",
    cc: "Canales de comunicación",
    st: "Sistema de transporte",
    ad: "Accesibilidad 'discapacidad'",
    ef: "Entorno físico"
  }

  comunidad = {
    mediaComunidad: "Promedio General Comunidad",
    e: "Empleo",
    h: "Hospitalidad",
    rse: "Rezago social y económico"
  }


  rand(min, max) {
    return min + Math.random() * (max - min);
  }

  get_random_color() {
    var h = this.rand(1, 360);
    var s = this.rand(0, 100);
    var l = this.rand(0, 100);
    return 'hsl(' + h + ',' + s + '%,' + l + '%)';
  }


  mayusPrimerLetra(cadena: string) {
    return cadena.charAt(0).toUpperCase() + cadena.slice(1);
  }


}