import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { IndicadorService } from '../services/indicadores.service';
declare var $: any;


@Component({
  moduleId: module.id,
  selector: 'localidades',
  templateUrl: '../templates/localidades.component.html',
})

export class LocalidadComponent {

  idDestino: string;
  sub: any;
  destinoDetails: Object = {};
  photoLayer: Object[] = [];

  tipo_localidad = ['', 'Pueblo Mágico', 'Destino prioritario', 'Candidato a Pueblo Mágico'];
  dataCharts: Object[] = [];

  options = {
    pointLabelFontSize: 20,
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    tooltips: {
      enabled: true,
      callbacks: {
        label: function (tooltipItem, data) {

          // 
          var dataset = data.datasets[tooltipItem.datasetIndex];
          var label = data.labels[tooltipItem.index];
          var indicador;
         
          var tabs = {
            mediaOferta: "Promedio General Oferta",
            cia: "Calidad de la infraestructura de acceso",
            ca: "Calidad de los atractivos",
            ca1: "Calidad de alojamiento",
            ca2: "Calidad de alimentos",
            esd: "Estado de los señalamientos para llegar al destino",
            egct: "Estado general de la calidad del transporte",
            feh: "Facilidad de encontrar el hospedaje",
            fea: "Facilidad de encontrar hospedaje (alternativo)",
            rcp: "Relación calidad precio",
            va: "Variedad de los atractivos",
            ph: "Precio Hoteles",
            pr: "Precio Restaurantes",
            pt: "Precio Transporte",
            mediaDemanda: "Promedio General Demanda",
            cat: "Relevancia de la atractividad",
            ata: "Porcentaje de visitantes extranjeros",
            ep: "Estadía promedio",
            mediaSeguridad: "Promedio General Seguridad",
            ps: "Percepción de seguridad",
            rn: "Riesgos naturales",
            mediaMercadotecnea: "Promedio General Mercadotécnia",
            ed: "Experiencia diferente",
            dit: "Disponibilidad de información turística",
            ev1: "Experiencia de viaje",
            ev2: "Emoción al visitar",
            pp: "Percepción de la promoción",
            mediaAccesibilidad: "Promedio General Accesibilidad",
            ci: "Canales de información",
            cc: "Canales de comunicación",
            st: "Sistema de transporte",
            ad: "Accesibilidad 'discapacidad'",
            ef: "Entorno físico",
            mediaComunidad: "Promedio General Comunidad",
            e: "Empleo",
            h: "Hospitalidad",
            vs1: "Valor del metro cuadro en venta",
            vs2: "Valor del metro cuadro en renta",
            rse: "Rezago social y económico"
          }

  
          for (var key in tabs) {
  
            if (tabs.hasOwnProperty(key)) {
               
              if (key == label){
            
                indicador = tabs[key];
                break;
              }
            }
          }

          return indicador +  " : " + dataset.data[tooltipItem.index];

        }
      }
    }
  };


  constructor(private router: ActivatedRoute, private elementRef: ElementRef, private indicadorService: IndicadorService) {
    this.idDestino = router.snapshot.params['id'];

  }

 //DESCOMENTAR
/*
var base;
var map_fotos;
var photoLayer;
*/
  ngOnInit(): void {

    this.sub = this.router.params.subscribe((params: Params) => {
      this.idDestino = params['id'];

      if (this.idDestino != null || this.idDestino != undefined){
        this.indicadorService.getDestinoValoraciones(this.idDestino).then(data => {
          $('#preloader').delay(0).fadeIn('fast');
          $('#status').fadeIn('fast');

          this.destinoDetails = data;
          this.fillGraficas();
          $('#status').fadeOut(550);
          $('#preloader').delay(550).fadeOut('slow');
        });

      this.indicadorService.getFotos(this.idDestino).then(function(data) {
        //DESCOMENTAR
/*
                      photoLayer.clear();

                      var photo = data;

                      var fotos = [];

                      for (var i = 0; i < photo.length; i++) {
                          if (photo[i].lat != null && photo[i].lon != null || photo[i].source != 'youtube') {
                              var fotoTmp = {
                                  lat: photo[i].lat,
                                  lng: photo[i].lon,
                                  url: photo[i].url,
                                  caption: '',
                                  thumbnail: photo[i].url,
                                  video: null
                              }
                              fotos.push(fotoTmp);
                          }
                      }

                      photoLayer.add(fotos).addTo(map_fotos);
*/
                  });


    }
    });
  }


  ngAfterViewInit () {

    //DESCOMENTAR
/*
		  base = new L.TileLayer(
        'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {});


    map_fotos = new L.Map('fotos', {
        layers: [base],
        center: new L.LatLng(23.4, -102.3),
        zoom: 4,
        maxZoom: 14,
        attributionControl: false,
        fullscreenControl: true,
        fullscreenControlOptions: { // optional
            title: "Ir a modo pantalla completa.",
            titleCancel: "Salir de modo pantalla completa"
        }

    });
	
	photoLayer = L.photo
        .cluster()
        .on(
            'click',
            function(evt) {
                var photo = evt.layer.photo,
                    template = '<img src="{url}" width="350" height="300" /></a><p>{caption}</p>';

                if (photo.video && (!!document
                        .createElement('video')
                        .canPlayType(
                            'video/mp4; codecs=avc1.42E01E,mp4a.40.2'))) {
                    template = '<video autoplay controls poster="{url}" width="300" height="300"><source src="{video}" type="video/mp4"/></video>';
                };

                evt.layer.bindPopup(L.Util.template(template, photo), {
                    className: 'leaflet-popup-photo',
                    minWidth: 350
                }).openPopup();
            });
 */
	}; 
 

 

  fillGraficas() {
    this.dataCharts = [];

    for (var key in this.tabs) {

      if (this.tabs.hasOwnProperty(key)) {
        //insertar objecto grafica por cada indicador

        this.dataCharts.push(this.procesar(this.tabs[key]));

      }
    }
  }

  procesar(dat) {
    var labels = [];
    var data = [];
    for (var key in dat) {

      if (dat.hasOwnProperty(key)) {
        labels.push(key);
        data.push(Math.floor(this.destinoDetails[key] * 100) / 100);
      }
    }

    var grafica = {
      labels: labels,
      datasets: [
        {

          data: data,
          backgroundColor: [
            this.get_random_color()

          ],

        }
      ]
    };
    return grafica;
  }
  rand(min, max) {
    return min + Math.random() * (max - min);
  }

  get_random_color() {
    var r = this.rand(255, 50);
    var g = this.rand(255, 50);
    var b = this.rand(255, 50);
    return 'rgba(' + r + ',' + g + ',' + b + ', 0.5)';
  }



  tabs = {
    "oferta": {
      mediaOferta: "Promedio General Oferta",
      cia: "Calidad de la infraestructura de acceso",
      ca: "Calidad de los atractivos",
      ca1: "Calidad de alojamiento",
      ca2: "Calidad de alimentos",
      esd: "Estado de los señalamientos para llegar al destino",
      egct: "Estado general de la calidad del transporte",
      feh: "Facilidad de encontrar el hospedaje",
      fea: "Facilidad de encontrar hospedaje (alternativo)",
      rcp: "Relación calidad precio",
      va: "Variedad de los atractivos",
      ph: "Precio Hoteles",
      pr: "Precio Restaurantes",
      pt: "Precio Transporte"
    },


    "demanda ": {
      mediaDemanda: "Promedio General Demanda",
      cat: "Relevancia de la atractividad",
      ata: "Porcentaje de visitantes extranjeros",
      ep: "Estadía promedio"
    },


    "seguridad": {
      mediaSeguridad: "Promedio General Seguridad",
      ps: "Percepción de seguridad",
      rn: "Riesgos naturales"
    },

    "mercadotecnia": {
      mediaMercadotecnea: "Promedio General Mercadotécnia",
      ed: "Experiencia diferente",
      dit: "Disponibilidad de información turística",
      ev1: "Experiencia de viaje",
      ev2: "Emoción al visitar",
      pp: "Percepción de la promoción"
    },

    "accesibilidad": {
      mediaAccesibilidad: "Promedio General Accesibilidad",
      ci: "Canales de información",
      cc: "Canales de comunicación",
      st: "Sistema de transporte",
      ad: "Accesibilidad 'discapacidad'",
      ef: "Entorno físico"
    },

    "comunidad": {
      mediaComunidad: "Promedio General Comunidad",
      e: "Empleo",
      h: "Hospitalidad",
      rse: "Rezago social y económico"
    }

  }


}