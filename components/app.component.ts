import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from '../services/config.service';
declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'app-home',
  templateUrl: '../templates/app.component.html'

})

//href="./logout"
export class AppComponent {

  destinos: Object[];
  router: any = null;
  loading: boolean = true;
  selectedId: any;

  hideNavBar(value: any): boolean {
 
   var substring = "reporte";
console.log( this.router.url.indexOf(substring));
  console.log(this.router.url);
console.log("xxxxxxxxxxxxx");
    // console.log(this.router.url == "/reporte");
    if (this.router.url.indexOf(substring) == 1)
      return false;
    else
      return true;
  }


getClass(){
 var substring = "reporte";
   if (this.router.url.indexOf(substring) == 1)
      return "";  
    else
      return "right_col";
  

}

  hideSearchBar(): boolean {

    if (this.router.url == "/usuarios" || this.router.url == "/configuracion" || this.router.url == "/aplicacion")
      return true;
    else
      return false;

  }

  constructor(private _router: Router, private configService: ConfigService) {
    this.router = _router;
  }


  ngOnInit(): void {
    this.destinos = [];
    this.configService.getDestinos().then(data => {
      this.destinos = data;
      this.loading = false;
    });

  }

  getName(obj: any): string {

    if (obj.municipioNombre == null || obj.municipioNombre == undefined)
      return this.transform(obj.localidad);
    else
      return this.transform(obj.municipioNombre);

  }

  transform(value: any) {
    if (value)
      return value.charAt(0).toUpperCase() + value.slice(1);
  }

  irADetalle(value: any) {

    if (value == undefined || value == null)
      return;


    if (this.router.url.search("servicios") == 1)
      return this.router.navigate(['/servicios', value]);

    else if (this.router.url.search("comentarios") == 1)
      return this.router.navigate(['/comentarios', value]);

    else if (this.router.url.search("visitantes") == 1)
      return this.router.navigate(['/visitantes', value]);

    else if (this.router.url.search("reporte") == 1)
      return this.router.navigate(['/reporte', value]);

    else (this.router.url.search("localidades") == 1)
    return this.router.navigate(['/localidades', value]);

  }


  irAServicios() {

    if (this.selectedId == undefined || this.selectedId == null)
      this.router.navigate(['/servicios']);
    else
      this.router.navigate(['/servicios', this.selectedId]);
  }


  irALocalidades() {

    if (this.selectedId == undefined || this.selectedId == null)
      this.router.navigate(['/localidades']);
    else
      this.router.navigate(['/localidades', this.selectedId]);
  }


  irAComentarios() {

    if (this.selectedId == undefined || this.selectedId == null)
      this.router.navigate(['/comentarios']);
    else
      this.router.navigate(['/comentarios', this.selectedId]);
  }



  irAVisitantes() {

    if (this.selectedId == undefined || this.selectedId == null)
      this.router.navigate(['/visitantes']);
    else
      this.router.navigate(['/visitantes', this.selectedId]);
  }

  irAReporte() {


    if (this.selectedId == undefined || this.selectedId == null)
      return;
    else {
      window.open('/secturv2/#/reporte/'+this.selectedId);
    }
    // window.open('/secturv2/#/reporte/'+this.selectedId);
  }

 

}
