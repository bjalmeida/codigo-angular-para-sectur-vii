import { Component, OnInit } from '@angular/core';
import { Configuracion } from '../class/Configuracion';
import { ConfigService } from '../services/config.service';

import { Response } from '@angular/http';
declare var $: any;


@Component({
  moduleId: module.id,
  selector: 'config',

  templateUrl: '../templates/configuracion.component.html'

})

export class ConfiguracionComponent implements OnInit {

  public cssColor: string = "#F0F0F0 !important";

  busbuster: string = "";
  miescape: string = "";
  blablacar: string = "";

  selectedLocations: number[] = [];
  deleteEnable: boolean = false;

  editionEnable: boolean = false; // usado para editar campos
  newLocation: boolean = false; //usada para determinar si es edicion de localidad o se agregara una nueva localidad
  localidades: Configuracion[];
  locationDetail: Configuracion = new Configuracion;

  tipo_localidad = ['', 'Pueblo Mágico', 'Destino prioritario', 'Candidato a Pueblo Mágico'];


  constructor(private configService: ConfigService) {
  }

  ngOnInit(): void {
    this.fetch();

  }

  setCssColor() {
    if (this.editionEnable) {
      return "blancoBg";
    } else {
      return "grisBg "; //whatever color
    }
  }

  selectConfig(position: number): void {

    if (this.selectedLocations.indexOf(position) >= 0) {
      this.selectedLocations.splice(this.selectedLocations.indexOf(position), 1);
    }
    else
      this.selectedLocations.push(position);

  }

  deleteConfirm(): void {
    this.deleteEnable = true;
  }

  deleteConfig(): void {

    let idLocation: string[] = [];

    for (let i = 0; i < this.selectedLocations.length; i++) {

      idLocation.push(this.localidades[this.selectedLocations[i]].id);

    }


    this.configService.deleteLocation(idLocation).then(response => this.fetch());
    this.selectedLocations = [];
    this.editionEnable = false;
    this.locationDetail = new Configuracion;
  }

  editConfig(localDetail: Configuracion): void {
    this.deleteEnable = false;
    this.locationDetail = localDetail;
    this.newLocation = false;
    this.setType(localDetail.tipoDestino);
    this.editionEnable = true;
    this.updateDestinos();
  }

  saveConfig(): void {
    if (this.deleteEnable == false) {

      if (this.locationDetail.lat != null && this.locationDetail.lon != null)
        this.locationDetail.location = this.locationDetail.lat + "," + this.locationDetail.lon;

      this.locationDetail.blablacarDestino = this.blablacar.split("\n");
      this.locationDetail.miescapeDestino = this.miescape.split("\n");
      this.locationDetail.busbusterDestino = this.busbuster.split("\n");

    }

    if (this.deleteEnable == true) {
      this.deleteConfig()
      this.deleteEnable = false;
    } else if (this.newLocation == false) {

      delete this.locationDetail['ultimaRevision'];
      this.configService.updateLocation(this.locationDetail).then(response => this.fetch());
    } else {


      this.configService.createLocation(this.locationDetail).then(response => this.fetch());
    }
    this.selectedLocations = [];

    this.updateDestinos();

  }

  updateDestinos() {

    if (this.locationDetail.busbusterDestino != null)
      this.busbuster = this.locationDetail.busbusterDestino.toString().replace(/,/g, "\n");
    else
      this.busbuster = ""
    if (this.locationDetail.miescapeDestino != null)
      this.miescape = this.locationDetail.miescapeDestino.toString().replace(/,/g, "\n");
    else
      this.miescape = ""
    if (this.locationDetail.blablacarDestino != null)
      this.blablacar = this.locationDetail.blablacarDestino.toString().replace(/,/g, "\n");
    else
      this.blablacar = ""
  }

  agregarConfig(): void {
    this.deleteEnable = false;
    this.newLocation = true;
    this.locationDetail = new Configuracion;
    this.editionEnable = true;

  }
  d
  // fetch all locs
  fetch(): void {
    this.localidades = [];
    this.configService.getAllLocations();
    this.configService.configChanged.subscribe((localidades: Configuracion[]) => {
      this.localidades = localidades;

      $('#status').fadeOut(550);
      $('#preloader').delay(550).fadeOut('slow');
    });
  }

  //para tipo de localidad
  setType(i: number): void {

    this.locationDetail.tipoDestino = i;

  }

  addType(tipo: string): void {

    this.tipo_localidad.push(tipo);

  }

  deleteType(position_type: number): void {

    this.tipo_localidad.splice(position_type, 1);
  }


  modType(position_type: number, type: string): void {

    this.tipo_localidad[position_type] = type;

  }
}