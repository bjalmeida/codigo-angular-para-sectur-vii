import { Component, OnInit } from '@angular/core';
import { Configuracion } from '../class/Configuracion';
import { ConfigService } from '../services/config.service';
import { IMyOptions, IMyDateModel } from 'mydatepicker';

import { Response } from '@angular/http';
declare var $: any;

@Component({
	moduleId: module.id,
	selector: 'aplicacion',
	templateUrl: '../templates/aplicacion.component.html',
})

export class AplicacionComponent implements OnInit {



	private optionsFecha: IMyOptions = {
		// other options...
		dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
		monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
		todayBtnTxt: "Hoy",
		firstDayOfWeek: "mo",
		sunHighlight: true,
		dateFormat: 'dd/mm/yyyy',
		selectionTxtFontSize: '13px',
		height: '25px'
	};

	changes: boolean = false;
	deleteEnable: boolean = false;//
	keywords: Configuracion[];
	keywordDetail: Configuracion = new Configuracion;
	idIndicador: string[] = []; //id de indicadores a ser borrados
	claveTracking: number = null;// para identificar palabra clave del indicador
	toBeDeleted: number = null;
	editableArray: boolean[] = [];//para la edicion de indicadores
	initialDate: Date = new Date();
	logger: Array<Object>;
	logText:string = '';

	ngOnInit(): void {

		this.fetch();

	}

	constructor(private configService: ConfigService) {
	}



	onDateChanged(event: IMyDateModel) {

		//datepicker inicial
		if(event==undefined)
		return;

		$('#preloader').delay(0).fadeIn('fast');
		$('#status').fadeIn('fast');
		this.initialDate = event.jsdate;


		this.configService.getAppStatus(this.initialDate.getTime() / 1000).then(response => {
			this.logger = response;
		});

		$('#status').fadeOut(550);
		$('#preloader').delay(550).fadeOut('slow');
	}



	deleteConfirm(id: string, index: number): void {
		this.deleteEnable = true;
		this.idIndicador.push(id);
		this.toBeDeleted = index;

	}


	editIndicador(localDetail: Configuracion, index: number): void {
		this.deleteEnable = false;
		this.keywordDetail = localDetail;
		this.claveTracking = index;
		this.changes = true;
		this.editableArray[index] = true;

	}

	saveConfig(): void {


		if (this.deleteEnable == true && this.changes == true) {
			this.keywords.splice(this.toBeDeleted, 1);

			this.configService.deleteLocation(this.idIndicador).then(response => this.configService.updateAll(this.keywords))
				.then(response => { this.fetch(); });
		} else if (this.deleteEnable == false && this.changes == true) {
			this.configService.updateAll(this.keywords).then(response => this.fetch());
		} else if (this.deleteEnable == true && this.changes == false) {
			this.configService.deleteLocation(this.idIndicador).then(response => this.fetch());
		}
		this.toBeDeleted = null;
		this.deleteEnable = false;
		this.changes = false;
		this.keywordDetail = new Configuracion;
		this.idIndicador = [];
	}

	addIndicador(): void {
		this.changes = true;
		this.keywordDetail = new Configuracion;
		this.keywordDetail.nombre = "";
		this.keywordDetail.acronimo = "";
		this.keywordDetail.clave = [];
		this.editableArray.unshift(true);

		this.keywords.unshift(this.keywordDetail);
		this.claveTracking = 0;

	}

	clearSelect(): void {
		this.idIndicador = [];
		this.deleteEnable = false;
	}
	/*manejo de palabras clave de indicadores*/
	deleteClave(position: number): void {
		this.keywordDetail.clave.splice(position, 1);
		this.updateClave()
	}

	addClave(): void {

		if (this.keywordDetail.clave == null || this.keywordDetail.clave == undefined)
			this.keywords[this.claveTracking].clave = [];

		this.keywordDetail.clave.unshift("");
		this.updateClave()
	}

	updateClave(): void {
		this.keywords[this.claveTracking].clave = this.keywordDetail.clave;

	}

	fetch(): void {
		this.keywords = [];
		this.configService.getAllKeywords();
		this.configService.configChanged.subscribe((keywords: Configuracion[]) => {
			this.keywords = keywords;
			this.editableArray = [];

			for (var i = 0; i < this.keywords.length; i++) {
				this.editableArray.push(false);
			}

			$('#status').fadeOut(550);
			$('#preloader').delay(550).fadeOut('slow');
		});

		var hoy = parseInt((this.initialDate.getTime() / 1000).toFixed(0));
		this.configService.getAppStatus(hoy).then(response => {
			this.logger = response
		});


	}

	logEstilo(val: string): string {

		switch (val) {
			case "WARN":
				return "warnColor";

			case "ERROR":
				return "errorColor";

		}

	}


	/** logger */

	getLog(log:string){

		this.configService.getLog(log).then(data => {
			this.logText = data["_body"];
			 

		});
	}

}