"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Configuracion_1 = require('../class/Configuracion');
var config_service_1 = require('../services/config.service');
var ConfiguracionComponent = (function () {
    function ConfiguracionComponent(configService) {
        this.configService = configService;
        this.cssColor = "F0F0F0";
        this.selectedLocations = [];
        this.deleteEnable = false;
        this.editionEnable = false; // usado para editar campos
        this.newLocation = false; //usada para determinar si es edicion de localidad o se agregara una nueva localidad
        this.locationDetail = new Configuracion_1.Configuracion;
        this.tipo_localidad = ['Pueblo Mágico', 'Destino prioritario', 'Candidato a Pueblo Mágico'];
    }
    ConfiguracionComponent.prototype.ngOnInit = function () {
        this.fetch();
    };
    ConfiguracionComponent.prototype.selectConfig = function (position) {
        if (this.selectedLocations.indexOf(position) >= 0) {
            this.selectedLocations.splice(this.selectedLocations.indexOf(position), 1);
        }
        else
            this.selectedLocations.push(position);
    };
    ConfiguracionComponent.prototype.deleteConfirm = function () {
        this.deleteEnable = true;
    };
    ConfiguracionComponent.prototype.deleteConfig = function () {
        var _this = this;
        var idLocation = [];
        for (var i = 0; i < this.selectedLocations.length; i++) {
            idLocation.push(this.localidades[this.selectedLocations[i]].id);
        }
        this.cssColor = "F0F0F0";
        this.configService.deleteLocation(idLocation).then(function (response) { return _this.fetch(); });
        this.selectedLocations = [];
        this.editionEnable = false;
        this.locationDetail = new Configuracion_1.Configuracion;
    };
    ConfiguracionComponent.prototype.editConfig = function (localDetail) {
        this.deleteEnable = false;
        this.locationDetail = localDetail;
        this.newLocation = false;
        this.setType(localDetail.tipoDestino);
        this.editionEnable = true;
        this.cssColor = "000";
    };
    ConfiguracionComponent.prototype.saveConfig = function () {
        var _this = this;
        if (this.deleteEnable == true) {
            this.deleteConfig();
            this.deleteEnable = false;
        }
        else if (this.newLocation == false) {
            if (this.locationDetail.lat != null && this.locationDetail.lon != null)
                this.locationDetail.location = this.locationDetail.lat + "," + this.locationDetail.lon;
            this.configService.updateLocation(this.locationDetail).then(function (response) { return _this.fetch(); });
        }
        else {
            if (this.locationDetail.lat != null && this.locationDetail.lon != null)
                this.locationDetail.location = this.locationDetail.lat + "," + this.locationDetail.lon;
            this.configService.createLocation(this.locationDetail).then(function (response) { return _this.fetch(); });
        }
        this.selectedLocations = [];
        this.cssColor = "F0F0F0";
    };
    ConfiguracionComponent.prototype.agregarConfig = function () {
        this.deleteEnable = false;
        this.newLocation = true;
        this.locationDetail = new Configuracion_1.Configuracion;
        this.editionEnable = true;
        this.cssColor = "000";
    };
    // fetch all locs
    ConfiguracionComponent.prototype.fetch = function () {
        var _this = this;
        this.localidades = [];
        this.configService.getAllLocations();
        this.configService.configChanged.subscribe(function (localidades) { return _this.localidades = localidades; });
    };
    //para tipo de localidad
    ConfiguracionComponent.prototype.setType = function (i) {
        console.log("tipoDestino:" + this.locationDetail.tipoDestino);
        console.log("pos:" + (i));
        console.log(" this.cssColor:" + this.cssColor);
        this.locationDetail.tipoDestino = i;
    };
    ConfiguracionComponent.prototype.addType = function (tipo) {
        this.tipo_localidad.push(tipo);
    };
    ConfiguracionComponent.prototype.deleteType = function (position_type) {
        this.tipo_localidad.splice(position_type, 1);
    };
    ConfiguracionComponent.prototype.modType = function (position_type, type) {
        this.tipo_localidad[position_type] = type;
    };
    ConfiguracionComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'config',
            templateUrl: '../templates/configuracion.component.html'
        }), 
        __metadata('design:paramtypes', [config_service_1.ConfigService])
    ], ConfiguracionComponent);
    return ConfiguracionComponent;
}());
exports.ConfiguracionComponent = ConfiguracionComponent;
//# sourceMappingURL=configuracion.component.js.map