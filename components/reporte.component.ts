import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { IndicadorService } from '../services/indicadores.service';
declare var $: any;


@Component({
    moduleId: module.id,
    selector: 'reporte',
    templateUrl: '../templates/reporte.component.html',
})

export class ReporteComponent {
    idDestino: string;
    tipo_localidad = ['', 'Pueblo Mágico', 'Destino prioritario', 'Candidato a Pueblo Mágico'];
    sub: any;
    eval: any = {}; //datos del destino turisto del reporte
    comp: any = {}; //datos del mejor destino turistico para la comparativa
    date = new Date();

    today = this.date.getDate() + "/" + (this.date.getMonth() + 1) + "/" + this.date.getFullYear() + "  " + this.date.getHours() + ":" + this.date.getMinutes() + ":" + this.date.getSeconds();
    chartGeneral: any = {};
    dataCharts: Object[] = []; //graficas para el destino
    compCharts: Object[] = []; //grafica comparativas
    constructor(private router: ActivatedRoute, private elementRef: ElementRef, private indicadorService: IndicadorService) {
        this.idDestino = router.snapshot.params['id'];

    }


    ngOnInit(): void {

      $('#status').fadeOut(1);
      $('#preloader').delay(1).fadeOut('slow');

        this.sub = this.router.params.subscribe((params: Params) => {
            this.idDestino = params['id'];

            if (this.idDestino != null || this.idDestino != undefined) {
                //FALTA FECHAS DE CONSULTA SOLR 

                this.indicadorService.getReporte(this.idDestino).then(data => {
                    this.eval = data;
                    this.setGeneralChart();
                    this.createCharts();

                    this.indicadorService.getBestType(this.eval.tipoDestino).then(dataE => {

                        this.comp = dataE;

                        this.createChartsComparativa();

                    });

                });


            }
        });
    }



    setGeneralChart() {

        var data = {
            labels: ["Oferta", "Demanda", "Seguridad", "Comunidad", "Mercadotécnia", "Accesibilidad"],
            datasets: [
                {

                    data: [this.eval["mediaOferta"], this.eval["mediaDemanda"], this.eval["mediaSeguridad"],
                    this.eval["mediaComunidad"], this.eval["mediaMercadotecnea"], this.eval["mediaAccesibilidad"]],
                    backgroundColor: [
                        "rgba(124, 181, 236, 0.5)"
                    ],

                }
            ]
        };
        this.chartGeneral = data;

    }


    createCharts() {

        this.dataCharts = [];


        for (var index = 0; index < this.indicadores.length; index++) {


            this.dataCharts.push(this.getCharts(this.indicadores[index]));

        }

    }


    createChartsComparativa() {


        this.compCharts = [];

        for (var index = 0; index < this.indicadores.length; index++) {
            this.compCharts.push(this.getChartsComparativa(this.indicadores[index]));

        }

    }


    getCharts(indicador) {

        var datos = [];
        var datosComp = [];
        var label = [];

        //redondeo de cifras, en caso de ser NaN = 0.0


        for (var index = 0; index < this[indicador].length; index++) {

            var skey = this[indicador][index];

            if (isNaN(this.eval[skey]) || this.eval[skey] === null || this.eval[skey] === undefined)
                datos.push(0.0);
            else
                datos.push((Math.floor(this.eval[skey] * 100) / 100));



        }



        //labels para graficas
        for (var index = 0; index < this[indicador].length; index++) {
            var skey = this[indicador][index];
            label.push(skey.toUpperCase());

        }


        var dataChart = {
            labels: label,
            datasets: [
                {

                    data: datos,
                    backgroundColor: [
                        "rgba(124, 181, 236, 0.5)"
                    ],

                }
            ]
        };




        return dataChart;

    }


    getChartsComparativa(indicador) {

        var datos = [];
        var datosComp = [];
        var label = [];

        //redondeo de cifras, en caso de ser NaN = 0.0
        
       


        for (var index = 0; index < this[indicador].length; index++) {

            var skey = this[indicador][index];

            if (isNaN(this.eval[skey]) || this.eval[skey] === null || this.eval[skey] === undefined)
                datos.push(0.0);
            else
                datos.push((Math.floor(this.eval[skey] * 100) / 100));


            //para la grafica comparativa  
            //los datos de la comparativa es devuelto en arreglo     
            if (isNaN(this.comp[0][skey]) || this.comp[0][skey] === null || this.comp[0][skey] === undefined)
                datosComp.push(0.0);
            else
                datosComp.push((Math.floor(this.comp[0][skey] * 100) / 100));


        }



        //labels para graficas
        for (var index = 0; index < this[indicador].length; index++) {
            var skey = this[indicador][index];
            label.push(skey.toUpperCase());

        }


        var dataChartComp = {
            labels: label,
            datasets: [{
                label: this.transform(this.eval.destino),
                backgroundColor: "rgba(124, 181, 236, 0.5)",
                borderColor: "rgba(124, 181, 236, 0.5)",
                pointBackgroundColor: "rgba(124, 181, 236, 0.5)",
                data: datos
            }, {
                label: this.transform(this.comp[0]["destino"]),
                backgroundColor: "rgba(255, 26, 26, 0.4)",
                borderColor: "rgba(255, 26, 26, 0.4)",
                pointBackgroundColor: "rgba(255, 26, 26, 0.4)",
                data: datosComp
            }
            ]
        };




        return dataChartComp;

    }




    getDescripcion(subindicador): string {

        var calificacion = this.eval[subindicador];
        if (calificacion == undefined)
            return;
        var evaluacion = this.getCalificacion(calificacion);

        if (this.evaluaciones[subindicador][evaluacion]["desc"] == "")
            return "Sin datos disponibles"
        return this.evaluaciones[subindicador][evaluacion]["desc"];
    }

    getRecomendacion(subindicador): string {
        var calificacion = this.eval[subindicador];
        if (calificacion == undefined)
            return;
        var evaluacion = this.getCalificacion(calificacion);


        if (this.evaluaciones[subindicador][evaluacion]["rec"] == "")
            return "Sin datos disponibles"
        return this.evaluaciones[subindicador][evaluacion]["rec"];
    }

    getCalificacion(calificacion: number) {
        if (calificacion > 7 && calificacion <= 10)
            return "mb";
        else if (calificacion > 5 && calificacion <= 7)
            return "b";
        else if (calificacion >= 0 && calificacion <= 5)
            return "d";
        else
            return "na";
    }



    subindicadores = {
        //  mediaOferta: "Promedio General Oferta",
        cia: "Calidad de la infraestructura de acceso",
        ca: "Calidad de los atractivos",
        ca1: "Calidad de alojamiento",
        ca2: "Calidad de alimentos",
        esd: "Estado de los señalamientos para llegar al destino",
        egct: "Estado general de la calidad del transporte",
        feh: "Facilidad de encontrar el hospedaje",
        fea: "Facilidad de encontrar hospedaje (alternativo)",
        rcp: "Relación calidad precio",
        va: "Variedad de los atractivos",
        ph: "Precio Hospedaje",
        pr: "Precio Restaurantes",
        pt: "Precio Transporte",
        //   mediaDemanda: "Promedio General Demanda",
        cat: "Capacidad de atracción",
        // ata: "Porcentaje de visitantes extranjeros",
        ep: "Estadía promedio",
        //   mediaSeguridad: "Promedio General Seguridad",
        ps: "Percepción de seguridad",
        rn: "Riesgos naturales",
        //   mediaMercadotecnea: "Promedio General Mercadotécnia",
        ed: "Experiencia diferente",
        dit: "Disponibilidad de información turística",
        ev1: "Experiencia de viaje",
        ev2: "Emoción al visitar",
        pp: "Percepción de la promoción",
        //    mediaAccesibilidad: "Promedio General Accesibilidad",
        ci: "Canales de información",
        cc: "Canales de comunicación",
        st: "Sistema de transporte",
        ad: "Accesibilidad",
        ef: "Entorno físico",
        //     mediaComunidad: "Promedio General Comunidad",
        e: "Empleo",
        h: "Hospitalidad",
        vs1: "Valor del metro cuadro en venta",
        vs2: "Valor del metro cuadro en renta",
        rse: "Rezago social y económico"
    }
    //mb muy bien; b bien ; d deficiente
    //rec recomendacion ; desc descripcion, 

    evaluaciones = {

        cia: {
            mb: {
                desc: "No se han detectado comentarios que reflejan infraestructura de acceso negativa",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "Se han detectado comentarios que reflejan una percepción mala de la infraestructura de acceso.",
                rec: "Crear programas de aseguramiento de calidad ",
                prioridad: ""
            },

            d: {
                desc: "Se han encontrado comentarios que hacen referencia a la infraestructura de acceso de manera negativa dentro de las fuentes de internet",
                rec: "Realizar programas de mantenimiento para mejorar la infraestructura de acceso",
                prioridad: ""
            },

            na: {
                desc: "",
                rec: "",
                prioridad: ""
            }
        },
        ca: {
            mb: {
                desc: "La calidad de los atractivos es muy buena ",
                rec: "Recomendamos seguir con la estrategia del mantenimiento de los atractivos como hasta ahora ya que está reflejando una buena experiencia del visitante",
                prioridad: ""
            },

            b: {
                desc: "La calidad de los atractivos es aceptable, hay bastantes oportunidades de mejora",
                rec: "Se recomienda invertir en el mantenimiento de los atractivos turísticos",
                prioridad: ""
            },

            d: {
                desc: "Hay una deficiencia (en cuanto a estado físico y/o atractivita) generalizada en los atractivos lo que provoca  la mala valoración de los usuarios en las fuentes de internet",
                rec: "Se recomienda establecer como una prioridad a corto plazo invertir en la conservación de los atractivos, enfocando esfuerzos para mejorar la experiencia del usuario al visitar los atractivos del destino.",
                prioridad: ""
            },

            na: {
                desc: "No hay datos",
                rec: "Se recomienda impulsar al visitante a compartir su experiencia de viaje en los portales web especializados",
                prioridad: ""
            },
        },
        ca1: {
            mb: {
                desc: "La calidad de la infraestructura hotelera es muy buena",
                rec: "Se puede considerar que las acciones realizadas en cuanto al impulso al sector hotelero son buenas y presentan buenos resultados",
                prioridad: ""
            },

            b: {
                desc: "La calidad de la infraestructura hotelera es buena",
                rec: "Se recomienda implementar programas de monitoreo de calidad en los hoteles, aunque esta tarea se puede quedar como de baja prioridad",
                prioridad: ""
            },

            d: {
                desc: "Existe una deficiencia bastante importante en la infraestructura hotelera del destino.",
                rec: "Se recomienda impulsar programas para el mejoramiento de la infraestructura hotelera del destino",
                prioridad: ""
            },

            na: {
                desc: "No hay datos",
                rec: "Se recomienda impulsar al visitante a compartir su experiencia de viaje en los portales web especializados",
                prioridad: ""
            },
        },
        ca2: {
            mb: {
                desc: "La oferta gastronómica es excelente",
                rec: "La oferta gastronómica es una de las fortalezas del destino, los esfuerzos deben enfocarse a la difusión de esta y al monitoreo para que no decrezca en calidad",
                prioridad: ""
            },

            b: {
                desc: "La oferta gastronómica es buena",
                rec: "Existe una oportunidad de mejora en el sector gastronómico pero hasta ahora se presentan buenos resultados",
                prioridad: ""
            },

            d: {
                desc: "Existe una deficiencia en cuanto a la oferta gastronómica en destino",
                rec: "Se requiere impulsar el sector gastronómico en el destino",
                prioridad: ""
            },

            na: {
                desc: "No hay datos",
                rec: "Se recomienda impulsar al visitante a compartir su experiencia de viaje en los portales web especializados",
                prioridad: ""
            },
        },
        esd: {
            mb: {
                desc: "No se han obtenido comentarios que mala referencia de los señalamientos para llegar al destino",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "Se han obtenido cometarios dentro la redes sociales que reflejan un descuido en los señalamientos para llegar al destino",
                rec: "",
                prioridad: ""
            },

            d: {
                desc: "Se han obtenido cometarios dentro la redes sociales que reflejan una evaluación negativa en cuanto a los señalamientos para llegar al destino",
                rec: "Realizar programas de mantenimiento de la señalización para llegar al destino.", prioridad: ""
            },

            na: {
                desc: "",
                rec: "", prioridad: ""
            },
        },
        egct: {
            mb: {
                desc: "No Se han registrado comentarios dentro de redes sociales que reflejan una degradación importante en la calidad del transporte",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "Se han registrado comentarios dentro de redes sociales que reflejan una degradación en la calidad del transporte",
                rec: "",
                prioridad: ""
            },

            d: {
                desc: "Se han registrado comentarios dentro de redes sociales que reflejan una degradación muy importante en la calidad del transporte",
                rec: "Realizar programas de mejora del transporte",
                prioridad: ""
            },

            na: {
                desc: "",
                rec: "",
                prioridad: ""
            },
        },
        feh: {
            mb: {
                desc: "La oferta hotelera es muy buena",
                rec: "Existe un mercado bastante importante en cuanto a infraestructura hotelera, esto cuenta como una fortaleza del destino ya que existe oferta hotelera para todos los gustos y presupuestos",
                prioridad: ""
            },

            b: {
                desc: "La oferta hotelera es suficiente",
                rec: "La oferta hotelera es bastante y está bien difundida, hay oportunidad de mejora en caso de que el destino requiera más infraestructura hotelera",
                prioridad: ""
            },

            d: {
                desc: "Hay poca oferta hotelera o la difusión es escasa",
                rec: "Revisar si la oferta hotelera es suficiente para el destino, de ser así invertir en la difusión en portales especializados.",
                prioridad: ""
            },

            na: {
                desc: "No hay datos",
                rec: "Se recomienda impulsar al visitante a compartir su experiencia de viaje en los portales web especializados",
                prioridad: ""
            },
        },
        fea: {
            mb: {
                desc: "Gran presencia del hospedaje alternativo (Home Sharing)",
                rec: "Existe una gran oferta de home sharing en el destino, inclusive puede representar una competencia importante con el sector turístico lo cual es sano ya que el visitante tiene más oferta para el hospedaje",
                prioridad: ""
            },

            b: {
                desc: "Presencia incipiente en cuanto al hospedaje alternativo (home sharing) se refiere",
                rec: "Al parecer existe un crecimiento natural del home sharing , esto a grandes rasgos beneficia al destino, si la legislación lo permite dejemos que siga esta desarrollo.",
                prioridad: ""
            },

            d: {
                desc: "Baja presencia de hospedaje alternativo (home sharing)",
                rec: "Si está dentro del marco legislativo del destino se recomienda impulsar el crecimiento del modelo de hospedaje airbnb (home sharing)",
                prioridad: ""
            },

            na: {
                desc: "No hay datos",
                rec: "No se ha registrado actividad del tipo Home Sharing en el destino turístico",
                prioridad: ""
            },
        },
        rcp: {
            mb: {
                desc: "Según las evaluaciones de los usuarios la experiencia vivida en el destino turístico supera con respecto a lo pagado",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "Según las evaluaciones de los usuarios lo pagado en el destino turístico cumple con las expectativas",
                rec: "",
                prioridad: ""
            },

            d: {
                desc: "Según las evaluaciones del usuario la experiencia vivida en el destino está por debajo de lo pagado",
                rec: "",
                prioridad: ""
            },

            na: {
                desc: "No hay datos",
                rec: "Se recomienda impulsar al visitante a compartir su experiencia de viaje en los portales web especializados", prioridad: ""
            },
        },
        va: {
            mb: {
                desc: "Hay una gran variedad de atractivos turísticos",
                rec: "Existe una oferta muy variada de destinos turísticos esto es una gran fortaleza del destino turístico.",
                prioridad: ""
            },

            b: {
                desc: "Hay una buena variedad de los atractivos",
                rec: "En el destino turístico cuenta con un buen número de atractivos turísticos.",
                prioridad: ""
            },

            d: {
                desc: "La variedad de los atractivos es escasa",
                rec: "Difundir en las webs especializadas todos los atractivos que existen en el desino",
                prioridad: ""
            },

            na: {
                desc: "No hay datos",
                rec: "Se recomienda impulsar al visitante a compartir su experiencia de viaje en los portales web especializados", prioridad: ""
            },
        },
        ph: {
            mb: {
                desc: "La mayoría de los hospedajes que existen en el destino entran en la categoría de 4 a 5 estrellas",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "El hospedaje en el destino cuenta con un precio razonable ",
                rec: "Dependiendo del perfil de turista que visita el destino se puede crear una estrategia para atraer el tipo de oferta hotelera según los objetivos del destino",
                prioridad: ""
            },

            d: {
                desc: "El hospedaje cuenta con hospedaje económico en general ",
                rec: "Trabajar en el monitoreo y aseguramiento de la calidad ya que hospedaje económico y de calidad pueden ser exhibidos como una fortaleza del destino",
                prioridad: ""
            },

            na: {
                desc: "No hay datos",
                rec: "Se recomienda asegurarse que la oferta hotelera tenga visibilidad online", prioridad: ""
            },

        },
        pr: {
            mb: {
                desc: "Los precios de los alimentos en el destino son en general elevados",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "Los precios en los restaurantes del destino son razonables",
                rec: "",
                prioridad: ""
            },

            d: {
                desc: "Los precios en los restaurantes son económicos",
                rec: "",
                prioridad: ""
            },

            na: {
                desc: "No hay datos",
                rec: "Se recomienda impulsar al visitante a compartir su experiencia de viaje en los portales web especializados", prioridad: ""
            },
        },
        pt: {
            mb: {
                desc: "El costo para llegar al destino es muy alto partiendo desde las principales ciudades o los centros emisores",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "El costo para llegar al destino es razonable partiendo desde las principales ciudades o centros emisores",
                rec: "",
                prioridad: ""
            },

            d: {
                desc: "El costo para trasladarse al destino es bajo desde las principales ciudades o centros emisores.",
                rec: "",
                prioridad: ""
            },

            na: {
                desc: "No hay datos",
                rec: "Se recomienda impulsar al visitante a compartir su experiencia de viaje en los portales web especializados", prioridad: ""
            },
        },

        cat: {
            mb: {
                desc: "El destino ha incrementado significativamente el volumen de sus búsquedas en internet",
                rec: "Continuar con las acciones de promoción turística que han llevado a estos resultados",
                prioridad: ""
            },

            b: {
                desc: "El destino ha mantenido el volumen de búsquedas en internet",
                rec: "Fortalecer las estrategias de promoción en internet",
                prioridad: ""
            },

            d: {
                desc: "El destino ha sufrido una caída en las búsquedas dentro de internet durante el periodo",
                rec: "Fortalecer de manera agresiva las estrategias de promoción en internet ",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },

        ep: {
            mb: {
                desc: "Refleja que el número de días hospedados es mayor al promedio de los demás destinos",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "",
                rec: "Refleja el tiempo que se hospedan los visitantes esta dentro de la media de los destinos ",
                prioridad: ""
            },

            d: {
                desc: "Esto refleja una que los visitantes se hospedan pocos días en el destino",
                rec: "",
                prioridad: ""
            },

            na: {
                desc: "",
                rec: "", prioridad: ""
            },
        },

        ps: {
            mb: {
                desc: "No se han encontrado comentarios referentes a problemas de inseguridad en el destino",
                rec: "Continuar como hasta ahora con las preventivas de seguridad en el destino",
                prioridad: ""
            },

            b: {
                desc: "Refleja que el destino ha tenido algunos problemas de inseguridad , sin que esto impacte significativamente en la reputación del destino",
                rec: "Reforzar las acciones de seguridad en la entidad",
                prioridad: ""
            },

            d: {
                desc: "Refleja un alto número de comentarios que hacen referencia a problemas de inseguridad en el destino ",
                rec: "Reforzar de manera prioritaria las acciones de seguridad en la entidad",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },
        rn: {
            mb: {
                desc: "No se han encontrado comentarios referentes a problemas de riesgos naturales  en el destino",
                rec: "Continuar como hasta ahora con las preventivas de seguridad en el destino",
                prioridad: ""
            },

            b: {
                desc: "Refleja que el destino ha tenido algunos problemas referentes a riesgos naturales sin que esto impacte significativamente en la reputación del destino",
                rec: "Reforzar las acciones de protección civil",
                prioridad: ""
            },

            d: {
                desc: "Refleja un alto número de comentarios que hacen referencia a riesgos naturales en el destino ",
                rec: "Reforzar de manera prioritaria las acciones de protección civil",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },

        ed: {
            mb: {
                desc: "Se han encontrado comentarios donde se refleja una experiencia muy positiva en el destino",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "No se han encontrado comentarios relacionados con la experiencia vivida por el usuario",
                rec: "Alentar al visitante a compartir su experiencia de viaje en fuentes de internet",
                prioridad: ""
            },

            d: {
                desc: "Se han encontrado comentarios que hacen referencia a una mala experiencia del visitante en el destino turístico",
                rec: "Alentar al visitante a compartir su experiencia de viaje en fuentes de internet",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },
        dit: {
            mb: {
                desc: "Se ha encontrado bastante información en las fuentes de internet acerca de información turística",
                rec: "Continuar con las estrategias de difusión llevadas a cabo hasta ahora",
                prioridad: ""
            },

            b: {
                desc: "Se ha encontrado poca información turística acerca del destino en fuentes de internet",
                rec: "Impulsar la promoción de turística en medios electrónicos del destino",
                prioridad: ""
            },

            d: {
                desc: "No se he encontrado información turística referente al destino en fuentes de internet",
                rec: "Impulsar de manera agresiva la promoción de turística en medios electrónicos del destino",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },
        ev1: {
            mb: {
                desc: "Los atractivos son bastante fotografiados de manera espontánea, causada por la emoción que generan los atractivos",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "Los atractivos son algo fotografiados de manera espontánea, causada por la emoción que generan los atractivos",
                rec: "Alentar al visitante a compartir su experiencia de viaje en fuentes de internet",
                prioridad: ""
            },

            d: {
                desc: "Los atractivos son poco fotografiados  y compartidos posteriormente",
                rec: "Alentar al visitante a compartir su experiencia de viaje en fuentes de internet",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },
        ev2: {
            mb: {
                desc: "Los atractivos son bastante fotografiados de manera espontánea, causada por la emoción que generan los atractivos",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "Los atractivos son algo fotografiados de manera espontánea, causada por la emoción que generan los atractivos",
                rec: "Alentar al visitante a compartir su experiencia de viaje en fuentes de internet",
                prioridad: ""
            },

            d: {
                desc: "Los atractivos son poco fotografiados de manera espontánea, causada por la emoción que generan los atractivos",
                rec: "Alentar al visitante a compartir su experiencia de viaje en fuentes de internet",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },
        pp: {
            mb: {
                desc: "La percepción turístico es positiva",
                rec: "Continuar con las estrategias de promoción turística llevadas a cabo hasta ahora",
                prioridad: ""
            },

            b: {
                desc: "La percepción de la promoción turística es neutra",
                rec: "Mejorar los contenidos de la promoción turística",
                prioridad: ""
            },

            d: {
                desc: "La percepción de la promoción es en su mayoría negativa",
                rec: "Mejorar de manera significativa los contenidos de la promoción turística",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },

        ci: {
            mb: {
                desc: "El destino es muy activo en las fuentes de internet",
                rec: "Continuar con la estrategia del destino turístico en cuanto a difusión en redes sociales se refiere",
                prioridad: ""
            },

            b: {
                desc: "El destino es medianamente activo en las fuentes de internet",
                rec: "Aumentar la presencia del destino en redes sociales con el objetivo de fortalecer la promoción turística",
                prioridad: ""
            },

            d: {
                desc: "El destino es poco activo o nulamente activo en las fuentes de internet",
                rec: "Promover la participación del destino en redes sociales con el objetivo de fortalecer la promoción turística",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },
        cc: {
            mb: {
                desc: "El destino cuenta con una página de internet con bastante información turística en ella ",
                rec: "Continuar con la estrategia digital actual",
                prioridad: ""
            },
            b: {
                desc: "El destino cuenta con página de internet pero cuanta con poco no nada de contenido turístico dentro de ella",
                rec: "Mejorar el contenido de la página actual incorporando un apartado de destino en el menú principal",
                prioridad: ""
            },

            d: {
                desc: "El destino no cuenta con página de internet o está constantemente como no accesible",
                rec: "Invertir en el desarrollo de una página de internet y la infraestructura necesaria para promoción del destino",
                prioridad: ""
            },
            na: { desc: "", rec: "", prioridad: "" },
        },
        st: {
            mb: {
                desc: "Existe una amplia oferta de transporte para llegar al destino",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "Existe un flujo regular de transporte al destino",
                rec: "",
                prioridad: ""
            },

            d: {
                desc: "Existe poco flujo de transporte en el destino",
                rec: "",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },
        ad: {
            mb: {
                desc: "La percepción del visitante en cuanto a la accesibilidad para personas con discapacidad es buena",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "La percepción del visitante en cuanto a la accesibilidad para personas con discapacidad es neutra",
                rec: "Realizar inversión en  infraestructura para facilitar el acceso a gente con discapacidad",
                prioridad: ""
            },

            d: {
                desc: "Según la percepción de los visitantes el destino no es accesible para personas con discapacidad",
                rec: "Realizar inversión en  infraestructura para facilitar el acceso a gente con discapacidad",
                prioridad: ""
            },

            na: {
                desc: "", rec: "", prioridad: ""
            },
        },
        ef: {
            mb: {
                desc: "La percepción del entorno físico del destino es  muy buena",
                rec: "Continuar con las estrategias de conservación de imagen urbana que hasta ahora se llevan acabo",
                prioridad: ""
            },

            b: {
                desc: "La percepción del entorno físico del destino es en general buena",
                rec: "Impulsar programas de mejoramiento de la imagen urbana del destino",
                prioridad: ""
            },

            d: {
                desc: "La percepción del entorno físico del destino en general es negativa",
                rec: "Impulsar de manera prioritaria programas de mejoramiento de la imagen urbana del destino",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },

        e: {
            mb: {
                desc: "Según la información del DENUE es de los destinos con más potencial de generar empleo ",
                rec: "Continuar con las políticas públicas que le han permitido generar estos empleos",
                prioridad: ""
            },

            b: {
                desc: "Según el DENUE el potencial de empleos generados en el destino está dentro del rango promedio ",
                rec: "Impulsar políticas que permitan generar empleos en el destino",
                prioridad: ""
            },

            d: {
                desc: "Según el DENUE el potencial total de empleos dentro del destino es menor al promedio entre ellos",
                rec: "El Impulsar políticas que generen empleos en el destino es de alta prioridad",
                prioridad: ""
            },


            na: { desc: "", rec: "", prioridad: "" },
        },
        h: {
            mb: {
                desc: "Se han encontrado comentarios que reflejen hostilidad por parte de la comunidad  hacia el viajero ",
                rec: "Enfocar esfuerzos para mejorar que la experiencia de viaje en el destino mejore",
                prioridad: ""
            },

            b: {
                desc: "No se han encontrado comentarios que reflejen temas de hospitalidad",
                rec: "Alentar al visitante a compartir su experiencia de viaje en fuentes de internet",
                prioridad: ""
            },

            d: {
                desc: "Se han encontrado comentarios que reflejen hostilidad por parte de la comunidad  hacia el viajero ",
                rec: "Enfocar esfuerzos para mejorar que la experiencia de viaje en el destino mejore",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },
        vs1: {
            mb: {
                desc: "El suelo en venta en la entidad tiene una apreciación alta con respecto a los demás destinos",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "El suelo en venta en la entidad tiene una apreciación media con respecto a los demás destinos",
                rec: "",
                prioridad: ""
            },

            d: {
                desc: "El suelo en venta en la entidad tiene una apreciación baja con respecto a los demás destinos",
                rec: "",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },
        vs2: {
            mb: {
                desc: "El suelo en renta en la entidad tiene una apreciación alta con respecto a los demás destinos",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "El suelo en renta en la entidad tiene una apreciación media con respecto a los demás destinos",
                rec: "",
                prioridad: ""
            },

            d: {
                desc: "El suelo en renta en la entidad tiene una apreciación baja con respecto a los demás destinos",
                rec: "",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        },
        rse: {
            mb: {
                desc: "Se ha encontrado evidencia dentro de los comentarios de que existe una mejora en las condiciones Sociales y económicas de la comunidad",
                rec: "",
                prioridad: ""
            },

            b: {
                desc: "No se han encontrado comentarios que reflejen el estado del rezago social y económico",
                rec: "Alentar al visitante a compartir su experiencia de viaje en fuentes de internet",
                prioridad: ""
            },

            d: {
                desc: "Se han encontrado comentarios que reflejen un Rezago social y económico en la comunidad  hacia el viajero",
                rec: "Enfocar esfuerzos para mejorar las condiciones sociales de la comunidad",
                prioridad: ""
            },

            na: { desc: "", rec: "", prioridad: "" },
        }
    }


    indicadores = ["oferta", "demanda", "seguridad", "comunidad", "mercadotecnia", "accesibilidad"];

    oferta =

    ["cia",
        "ca",
        "ca1",
        "ca2",
        "esd",
        "egct",
        "feh",
        "fea",
        "rcp",
        "va",
        "ph",
        "pr",
        "pt"];



    demanda =

    ["cat",
        "ep"];



    seguridad =

    ["ps",
        "rn"];


    mercadotecnia =

    ["ed",
        "dit",
        "ev1",
        "ev2",
        "pp"];


    accesibilidad =

    ["ci",
        "cc",
        "st",
        "ad",
        "ef"];


    comunidad = [

        "e",
        "h",
        "rse"
    ];



    options = {
        pointLabelFontSize: 20,
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        tooltips: {
            enabled: true,
            callbacks: {
                label: function (tooltipItem, data) {

                    // 
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var label = data.labels[tooltipItem.index];
                    var indicador;

                    var tabs = {
                        mediaOferta: "Promedio General Oferta",
                        cia: "Calidad de la infraestructura de acceso",
                        ca: "Calidad de los atractivos",
                        ca1: "Calidad de alojamiento",
                        ca2: "Calidad de alimentos",
                        esd: "Estado de los señalamientos para llegar al destino",
                        egct: "Estado general de la calidad del transporte",
                        feh: "Facilidad de encontrar el hospedaje",
                        fea: "Facilidad de encontrar hospedaje (alternativo)",
                        rcp: "Relación calidad precio",
                        va: "Variedad de los atractivos",
                        ph: "Precio Hoteles",
                        pr: "Precio Restaurantes",
                        pt: "Precio Transporte",
                        mediaDemanda: "Promedio General Demanda",
                        cat: "Relevancia de la atractividad",
                        ata: "Porcentaje de visitantes extranjeros",
                        ep: "Estadía promedio",
                        mediaSeguridad: "Promedio General Seguridad",
                        ps: "Percepción de seguridad",
                        rn: "Riesgos naturales",
                        mediaMercadotecnea: "Promedio General Mercadotécnia",
                        ed: "Experiencia diferente",
                        dit: "Disponibilidad de información turística",
                        ev1: "Experiencia de viaje",
                        ev2: "Emoción al visitar",
                        pp: "Percepción de la promoción",
                        mediaAccesibilidad: "Promedio General Accesibilidad",
                        ci: "Canales de información",
                        cc: "Canales de comunicación",
                        st: "Sistema de transporte",
                        ad: "Accesibilidad 'discapacidad'",
                        ef: "Entorno físico",
                        mediaComunidad: "Promedio General Comunidad",
                        e: "Empleo",
                        h: "Hospitalidad",
                        vs1: "Valor del metro cuadro en venta",
                        vs2: "Valor del metro cuadro en renta",
                        rse: "Rezago social y económico"
                    }


                    for (var key in tabs) {

                        if (tabs.hasOwnProperty(key)) {

                            if (key == label.toLowerCase()) {

                                indicador = tabs[key];
                                break;
                            }
                        }
                    }

                    if (indicador != undefined)
                        return indicador + " : " + dataset.data[tooltipItem.index];
                    else

                        return "" + (Math.floor(dataset.data[tooltipItem.index] * 100) / 100);
                }
            }
        }
    };


    optionsComp = {
        pointLabelFontSize: 20,
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: true
        },
        tooltips: {
            enabled: true,
             mode: 'label',
            callbacks: {
                label: function (tooltipItem, data) {

                    // 
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var label = data.labels[tooltipItem.index];
                    var indicador;

                    var tabs = {
                        mediaOferta: "Promedio General Oferta",
                        cia: "Calidad de la infraestructura de acceso",
                        ca: "Calidad de los atractivos",
                        ca1: "Calidad de alojamiento",
                        ca2: "Calidad de alimentos",
                        esd: "Estado de los señalamientos para llegar al destino",
                        egct: "Estado general de la calidad del transporte",
                        feh: "Facilidad de encontrar el hospedaje",
                        fea: "Facilidad de encontrar hospedaje (alternativo)",
                        rcp: "Relación calidad precio",
                        va: "Variedad de los atractivos",
                        ph: "Precio Hoteles",
                        pr: "Precio Restaurantes",
                        pt: "Precio Transporte",
                        mediaDemanda: "Promedio General Demanda",
                        cat: "Relevancia de la atractividad",
                        ata: "Porcentaje de visitantes extranjeros",
                        ep: "Estadía promedio",
                        mediaSeguridad: "Promedio General Seguridad",
                        ps: "Percepción de seguridad",
                        rn: "Riesgos naturales",
                        mediaMercadotecnea: "Promedio General Mercadotécnia",
                        ed: "Experiencia diferente",
                        dit: "Disponibilidad de información turística",
                        ev1: "Experiencia de viaje",
                        ev2: "Emoción al visitar",
                        pp: "Percepción de la promoción",
                        mediaAccesibilidad: "Promedio General Accesibilidad",
                        ci: "Canales de información",
                        cc: "Canales de comunicación",
                        st: "Sistema de transporte",
                        ad: "Accesibilidad 'discapacidad'",
                        ef: "Entorno físico",
                        mediaComunidad: "Promedio General Comunidad",
                        e: "Empleo",
                        h: "Hospitalidad",
                        vs1: "Valor del metro cuadro en venta",
                        vs2: "Valor del metro cuadro en renta",
                        rse: "Rezago social y económico"
                    }


                    for (var key in tabs) {

                        if (tabs.hasOwnProperty(key)) {

                            if (key == label.toLowerCase()) {

                                indicador = tabs[key];
                                break;
                            }
                        }
                    }

                    if (indicador != undefined)
                        return indicador + " : " + dataset.data[tooltipItem.index];
                    else

                        return "" + (Math.floor(dataset.data[tooltipItem.index] * 100) / 100);
                }
            }
        }
    };

    transform(value: any) {
        if (value) {
            return value.charAt(0).toUpperCase() + value.slice(1);
        }
        return value;
    }

    splitLabel(texto){
        var array = texto.split(" ");

      return array;

    }

}