import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { IndicadorService } from '../services/indicadores.service';
declare var $: any;
declare var jqcloud: any;




@Component({
  moduleId: module.id,
  selector: 'visitantes',
  templateUrl: '../templates/visitantes.component.html',
})

export class VisitantesComponent {
  idDestino: string;
  sub: any;
  chartAge: {};
  chartGender: {};

  optionsChart = {
    pointLabelFontSize: 20,
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: true
    }

  };

  photos: Object[] = [];

  constructor(private router: ActivatedRoute, private elementRef: ElementRef, private indicadorService: IndicadorService) {
    this.idDestino = router.snapshot.params['id'];

  }

 //DESCOMENTAR
/*
var base2;
var map_origen;
 
 
var info;
var map;
var markers;

function onEachFeature(feature, layer) {
    var popupContent = "<p>Lugar de origen: " +
        feature.properties.city + "</p><p>Número de visitantes:" + feature.properties.count + "</p>";

    if (feature.properties && feature.properties.popupContent) {
        popupContent += feature.properties.popupContent;
    }

    layer.bindPopup(popupContent);
}

*/

  ngOnInit(): void {


    this.sub = this.router.params.subscribe((params: Params) => {
      this.idDestino = params['id'];
      if (this.idDestino != undefined) {
 //DESCOMENTAR
/*
          _this.indicadorService.getTravelersCity(_this.idDestino).then(function(data) {

                var cityPoints = L.geoJSON(data, {

                    pointToLayer: function(feature, latlng) {
                        return L.marker(latlng);
                    },

                    onEachFeature: onEachFeature
                });

                markers.clearLayers();
                markers.addLayer(cityPoints);
				 
                map_origen.addLayer(markers);


            });



         
            _this.indicadorService.getViajeros(_this.idDestino).then(function(data) {
                var country = [];
                for (var key in data) {
                    if (data.hasOwnProperty(key)) {

                        var tmp = {
                            id: key,
                            value: data[key]
                        };
                        country.push(tmp);
                    }
                }


                map = AmCharts.makeChart("origen", {
                    "type": "map",
                    "theme": "light",
                    "colorSteps": 10,
                    language: "es",
                    "dataProvider": {
                        "map": "worldHigh",
                        "areas": country
                    },

                    "areasSettings": {
                        "autoZoom": true,
                        "balloonText": "Viajeros de [[title]]: [[value]]"
                    },

                    "valueLegend": {
                        "right": 10,
                        "minValue": "Pocos Visitantes",
                        "maxValue": "Más Visitantes"
                    },

                    "export": {
                        "enabled": true
                    }

                });
            });
            _this.indicadorService.getWordCloud(_this.idDestino).then(function(data) {
                //getWordCloud
                var words = [];
                for (var key in data) {
                    if (data.hasOwnProperty(key)) {
                        var tmp = {
                            text: key,
                            weight: data[key]
                        };
                        words.push(tmp);
                    }
                }

                $('#cloudWord').jQCloud(words);
                $('#cloudWord').jQCloud('update', words, {
                    autoResize: true
                });
            });

            */
        this.indicadorService.getTravelersGender(this.idDestino).then(data => {
          //genero
          this.chartGender = {
            labels: [
              "Mujeres",
              "Hombres"
            ],
            datasets: [
              {
                data: [data.female, data.male],
                backgroundColor: [
                  "#ff4d4d",
                  "#8080ff"
                ],
                hoverBackgroundColor: [
                  "#ff4d4d",
                  "#8080ff"
                ]
              }]
          };

        });

        this.indicadorService.getTravelersAge(this.idDestino).then(data => {
          //edad

          var categoria = ["13-17", "18-24", "25-34", "35-49", "50-64", "65+"];
          var datos = [];
          for (var i = 0; i < categoria.length; i++)
            if (data[categoria[i]] != undefined)
              datos.push(data[categoria[i]]);
            else
              datos.push(0);

          this.chartAge = {
            labels: [
              "Menor de 18 años",
              "Entre 18 y 24 años",
              "Entre 25 y 34 años",
              "Entre 35 y 49 años",
              "Entre 50 y 64 años",
              "Mayor de 65 años"

            ],
            datasets: [
              {
                data: datos,
                backgroundColor: [
                  "#ffff80",
                  "#ff4000",
                  "#80ff80",
                  "#80dfff",
                  "#8080ff",
                  "#bf80ff"

                ],
                hoverBackgroundColor: [
                  "#ffff80",
                  "#ff4000",
                  "#80ff80",
                  "#80dfff",
                  "#8080ff",
                  "#bf80ff"
                ]
              }]
          };
        });


      }
    });
  }

 //DESCOMENTAR
 ngAfterViewInit () {
/*

	markers = L.markerClusterGroup();

       base2 = new L.TileLayer(
        'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {});


   

      map_origen = new L.Map('origenCity', {
        layers: [base2],
        center: new L.LatLng(23.4, -102.3),
        zoom: 4,    maxZoom: 14,
        attributionControl: false,
        fullscreenControl: true,
        fullscreenControlOptions: { // optional
            title: "Ir a modo pantalla completa.",
            titleCancel: "Salir de modo pantalla completa"
        }

    });


   
   


    info = L.control();



    map = AmCharts.makeChart("origen", {
        "type": "map",
        "theme": "light",
        "colorSteps": 10,
        language: "es",
        "dataProvider": {
            "map": "worldHigh"

        },

        "areasSettings": {
            "autoZoom": true,

        },

        "valueLegend": {
            "right": 10,
            "minValue": "Pocos Visitantes",
            "maxValue": "Más Visitantes"
        },

        "export": {
            "enabled": true
        }

    });

 
*/

 }

}