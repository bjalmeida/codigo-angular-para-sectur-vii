"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Usuarios_1 = require('../class/Usuarios');
var config_service_1 = require('../services/config.service');
var UsuariosComponent = (function () {
    function UsuariosComponent(configService) {
        this.configService = configService;
        this.usuarioDetail = new Usuarios_1.Usuario;
        this.cssColor = "F0F0F0";
        this.selectedUsers = [];
        this.deleteEnable = false;
        this.editionEnable = false; // usado para editar campos
        this.newUser = false; //usada para determinar si es edicion de usuario o se agregara un usuairo nuevo
        this.rolList = ['Admin', 'Gestor App', 'Gestor Destino'];
    }
    UsuariosComponent.prototype.ngOnInit = function () {
        this.fetch();
        console.log(this.usuarios);
    };
    UsuariosComponent.prototype.fetch = function () {
        var _this = this;
        this.usuarios = [];
        this.configService.getAllUsers();
        this.configService.usersChanged.subscribe(function (usuarios) { return _this.usuarios = usuarios; });
    };
    UsuariosComponent.prototype.setType = function (i) {
        this.usuarioDetail.rol = i;
    };
    UsuariosComponent.prototype.selectUser = function (position) {
        if (this.selectedUsers.indexOf(position) >= 0) {
            this.selectedUsers.splice(this.selectedUsers.indexOf(position), 1);
        }
        else
            this.selectedUsers.push(position);
    };
    UsuariosComponent.prototype.deleteConfirm = function () {
        this.deleteEnable = true;
    };
    UsuariosComponent.prototype.deleteUser = function () {
        var _this = this;
        var idLocation = [];
        for (var i = 0; i < this.selectedUsers.length; i++) {
            idLocation.push(this.usuarios[this.selectedUsers[i]].id);
        }
        this.configService.deleteUser(idLocation).then(function (response) { return _this.fetch(); });
        this.selectedUsers = [];
        this.editionEnable = false;
        this.usuarioDetail = new Usuarios_1.Usuario;
    };
    UsuariosComponent.prototype.editUser = function (localDetail) {
        this.deleteEnable = false;
        this.usuarioDetail = localDetail;
        this.newUser = false;
        this.setType(localDetail.rol);
        this.editionEnable = true;
        this.cssColor = "000";
    };
    UsuariosComponent.prototype.saveUser = function () {
        var _this = this;
        if (this.deleteEnable == true) {
            this.deleteUser();
            this.deleteEnable = false;
        }
        else if (this.newUser == false) {
            this.configService.updateUser(this.usuarioDetail).then(function (response) { return _this.fetch(); });
        }
        else {
            this.configService.createUser(this.usuarioDetail).then(function (response) { return _this.fetch(); });
        }
        this.selectedUsers = [];
        this.cssColor = "F0F0F0";
    };
    UsuariosComponent.prototype.agregarUser = function () {
        this.deleteEnable = false;
        this.newUser = true;
        this.usuarioDetail = new Usuarios_1.Usuario;
        this.editionEnable = true;
        this.cssColor = "000";
    };
    UsuariosComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'users',
            templateUrl: '../templates/usuarios.component.html',
        }), 
        __metadata('design:paramtypes', [config_service_1.ConfigService])
    ], UsuariosComponent);
    return UsuariosComponent;
}());
exports.UsuariosComponent = UsuariosComponent;
//# sourceMappingURL=usuarios.component.js.map