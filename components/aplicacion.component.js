"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var AplicacionComponent = (function () {
    function AplicacionComponent() {
    }
    AplicacionComponent.prototype.logEstilo = function (val) {
        switch (val) {
            case "WARN":
                return "warnColor";
            case "ERROR":
                return "errorColor";
        }
    };
    AplicacionComponent.prototype.ngOnInit = function () {
        this.logger = [{
                hora: 'X/X/X hh:hh:hh',
                nivel: 'WARN',
                core: 'solrSEctur',
                logger: 'solrCore',
                fuente: 'tripadvisor',
                mensaje: 'fallo xxxxx razones...',
            }, {
                hora: 'X/X/X hh:hh:hh',
                nivel: 'ERROR',
                core: 'solrSEctur',
                logger: 'solrCore',
                fuente: 'flickr',
                mensaje: 'fallo xxxxx razones...',
            }, {
                hora: 'X/X/X hh:hh:hh',
                nivel: 'WARN',
                core: 'solrSEctur',
                logger: 'solrCore',
                fuente: 'tripadvisor',
                mensaje: 'fallo xxxxx razones...',
            }];
    };
    AplicacionComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'aplicacion',
            templateUrl: '../templates/aplicacion.component.html',
        }), 
        __metadata('design:paramtypes', [])
    ], AplicacionComponent);
    return AplicacionComponent;
}());
exports.AplicacionComponent = AplicacionComponent;
//# sourceMappingURL=aplicacion.component.js.map