import { Component, OnInit } from '@angular/core';
import { Usuario } from '../class/Usuarios';
import { ConfigService } from '../services/config.service';
import { Response } from '@angular/http';
declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'users',
  templateUrl: '../templates/usuarios.component.html',
})

export class UsuariosComponent implements OnInit {
  public cssColor: string = "#990202 !important";
  usuarios: Usuario[];
  usuarioDetail: Usuario = new Usuario;
  index: number = null;
  selectedUsers: number[] = [];
  deleteEnable: boolean = false;
  editionEnable: boolean = false; // usado para editar campos
  newUser: boolean = false; //usada para determinar si es edicion de usuario o se agregara un usuairo nuevo


  role = ['ROLE_ADMIN', 'ROLE_GP', 'ROLE_GD']
  rolList = ['Admin', 'Gestor App', 'Gestor Destino'];

  constructor(private configService: ConfigService) {
  }

  ngOnInit(): void {
    this.fetch();
    $('#status').fadeOut(550);
    $('#preloader').delay(550).fadeOut('slow');
  }

  setCssColor() {
    if (this.editionEnable) {
      return "blancoBg";
    } else {
      return "grisBg "; //whatever color
    }
  }

  fetch(): void {
    this.usuarios = [];
    this.configService.getAllUsers();
    this.configService.usersChanged.subscribe((usuarios: Usuario[]) => this.usuarios = usuarios);
  }

  setType(i: number): void {

    this.usuarioDetail.rol = this.role[i];
    this.index = i;

  }


  selectUser(position: number): void {

    if (this.selectedUsers.indexOf(position) >= 0) {
      this.selectedUsers.splice(this.selectedUsers.indexOf(position), 1);
    }
    else
      this.selectedUsers.push(position);
  }

  deleteConfirm(): void {
    this.deleteEnable = true;
  }

  deleteUser(): void {

    let idLocation: string[] = [];
    this.index = null;
    for (let i = 0; i < this.selectedUsers.length; i++) {

      idLocation.push(this.usuarios[this.selectedUsers[i]].id);

    }


    this.configService.deleteUser(idLocation).then(response => this.fetch());
    this.selectedUsers = [];
    this.editionEnable = false;
    this.usuarioDetail = new Usuario;

  }

  editUser(localDetail: Usuario): void {


    this.deleteEnable = false;
    this.usuarioDetail = localDetail;
    this.newUser = false;
    var i;
    for (i = 0; i < this.role.length; i++)
      if (localDetail.rol === this.role[i])
        break;
    this.setType(i);
    this.editionEnable = true;

  }

  getRol(rol: string): number {

    var i;
    for (i = 0; i < this.role.length; i++)
      if (rol === this.role[i])
        return i;
  }

  saveUser(): void {

    if (this.deleteEnable == true) {
      this.deleteUser()
      this.deleteEnable = false;
      this.index = null;
    } else if (this.newUser == false) {
      this.configService.updateUser(this.usuarioDetail).then(response => this.fetch());
    } else {
      this.configService.createUser(this.usuarioDetail).then(response => this.fetch());
    }
    this.selectedUsers = [];

  }

  agregarUser(): void {
    this.deleteEnable = false;
    this.newUser = true;
    this.usuarioDetail = new Usuario;
    this.editionEnable = true;
    this.index = null;
  }

}