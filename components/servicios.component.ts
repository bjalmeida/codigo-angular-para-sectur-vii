import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { IndicadorService } from '../services/indicadores.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Response } from '@angular/http';
import 'chart.js/src/chart';
declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'servicios',
  templateUrl: '../templates/servicios.component.html',
})

export class ServiciosComponent {
  idDestino: string;
  sub: any;
  labels = ["Rating", "Limpieza", "Ubicacion", "Calidad del descanso", "Calidad-Precio", "Estado de las habitaciones"];

  precioHotelChart = {};
  precioRestChart = {};

  evalHotelChart: Object[] = [];
  evalRestChart: Object[] = [];

  historicoHotelChart: Object[] = [];
  historicoRestChart: Object[] = [];


  hotelCategorySelector: number = 0;
  restCategorySelector: number = 0;

  categoriaRest = ["Sin categoria", "Económico", "Gama media", "Elegante"];
  categoriaHotel = ["Sin categoria", "1 estrella", "2 estrellas", "3 estrellas", "4 estrellas", "5 estrellas"];

  optionsBar = {
    tooltips: {
      enabled: true,
      callbacks: {
        label: function (tooltipItem, data) {

          var dataset = data.datasets[tooltipItem.datasetIndex];
          var truncateDecimal = Math.floor(dataset.data[tooltipItem.index] * 100) / 100;

          return dataset.label + ": $" + truncateDecimal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");//this.numberWithCommas(truncateDecimal);

        }
      }
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: true
      }]
    },
    legend: {
      display: false
    }
  };

  optionsRadar = {
    pointLabelFontSize: 20,
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false
    }
  };


  optionsLine = {
    pointLabelFontSize: 20,
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: true
    },
    scales: {
      xAxes: [{
        display: true
      }]
    },
    tooltips: {
      enabled: true,
      mode: 'label',
      callbacks: {
        label: function (tooltipItem, data) {

          var dataset = data.datasets[tooltipItem.datasetIndex];
          var truncateDecimal = Math.floor(dataset.data[tooltipItem.index] * 100) / 100;

          return dataset.label + ": $" + truncateDecimal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");//this.numberWithCommas(truncateDecimal);

        }
      }
    },
  };

  ngOnInit(): void {

    this.sub = this.router.params.subscribe((params: Params) => {

      this.hotelCategorySelector = 0;
      this.restCategorySelector = 0;

      this.idDestino = params['id'];

      if (this.idDestino != null || this.idDestino != undefined) {
        //precios Hoteles


        this.indicadorService.getHotelesEval(this.idDestino).then(data => {


          $('#preloader').delay(0).fadeIn('fast');
          $('#status').fadeIn('fast');
          var min = [], med = [], max = [];
          this.evalHotelChart = [];

          for (var i = 0; i < data.evaluaciones.length; i++) {

            min.push(Math.floor(data.evaluaciones[i]["min"] * 100) / 100);
            med.push(Math.floor(data.evaluaciones[i]["medium"] * 100) / 100);
            max.push(Math.floor(data.evaluaciones[i]["max"] * 100) / 100);

            this.evalHotelChart.push({
              labels: this.labels,
              datasets: [
                {

                  data: [Math.floor(data.evaluaciones[i]["rating"] * 100) / 100,
                  Math.floor(data.evaluaciones[i]["cleanliness"] * 100) / 100,
                  Math.floor(data.evaluaciones[i]["location"] * 100) / 100,
                  Math.floor(data.evaluaciones[i]["sleepQuality"] * 100) / 100,
                  Math.floor(data.evaluaciones[i]["values"] * 100) / 100,
                  Math.floor(data.evaluaciones[i]["rooms"] * 100) / 100],
                  backgroundColor: [
                    this.get_random_color()

                  ]
                }
              ]
            });

          }


          this.precioHotelChart = {
            labels: this.categoriaHotel,
            datasets: [
              {
                label: "Precio minimo",
                data: min,
                backgroundColor: [
                  "rgb(255, 255, 0,0.5)",
                  "rgb(255, 255, 0,0.6)",
                  "rgb(255, 255, 0,0.7)",
                  "rgb(255, 255, 0,0.8)",
                  "rgb(255, 255, 0,0.9)",
                  "rgb(255, 255, 0,1.0)"

                ],

              },
              {
                label: "Precio medio",
                data: med,
                backgroundColor: [
                  "rgb(0, 255, 0, 0.5)",
                  "rgb(0, 255, 0, 0.6)",
                  "rgb(0, 255, 0, 0.7)",
                  "rgb(0, 255, 0, 0.8)",
                  "rgb(0, 255, 0, 0.9)",
                  "rgb(0, 255, 0, 1.0)"

                ],

              }, {
                label: "Precio máximo",
                data: max,
                backgroundColor: [
                  "rgb(0, 153, 204, 0.5)",
                  "rgb(0, 153, 204, 0.6)",
                  "rgb(0, 153, 204, 0.7)",
                  "rgb(0, 153, 204, 0.8)",
                  "rgb(0, 153, 204, 0.9)",
                  "rgb(0, 153, 204, 1.0)"

                ],

              }

            ]
          };



          $('#status').fadeOut(550);
          $('#preloader').delay(550).fadeOut('slow');

        });

        //precios restaurantes

        this.indicadorService.getRestaurantsEval(this.idDestino).then(data => {

          var min = [], med = [], max = [];
          this.evalRestChart = [];

          for (var i = 0; i < data.evaluaciones.length; i++) {

            min.push(Math.floor(data.evaluaciones[i]["min"] * 100) / 100);
            med.push(Math.floor(data.evaluaciones[i]["medium"] * 100) / 100);
            max.push(Math.floor(data.evaluaciones[i]["max"] * 100) / 100);


            this.evalRestChart.push({
              labels: ["Rating", "Comida", "Calidad-Precio", "Ambiente", "Servicio"],
              datasets: [
                {

                  data: [Math.floor(data.evaluaciones[i]["rating"] * 100) / 100,
                  Math.floor(data.evaluaciones[i]["food"] * 100) / 100,
                  Math.floor(data.evaluaciones[i]["value"] * 100) / 100,
                  Math.floor(data.evaluaciones[i]["atmosphere"] * 100) / 100,
                  Math.floor(data.evaluaciones[i]["service"] * 100) / 100
                  ],
                  backgroundColor: [
                    this.get_random_color()

                  ]
                }
              ]
            });

          }


          this.precioRestChart = {
            labels: this.categoriaRest,
            datasets: [
              {
                label: "Precio minimo",
                data: min,
                backgroundColor: [

                  "rgb(255, 255, 0,0.7)",
                  "rgb(255, 255, 0,0.8)",
                  "rgb(255, 255, 0,0.9)",
                  "rgb(255, 255, 0,1.0)"

                ],

              },
              {
                label: "Precio medio",
                data: med,
                backgroundColor: [

                  "rgb(0, 255, 0, 0.7)",
                  "rgb(0, 255, 0, 0.8)",
                  "rgb(0, 255, 0, 0.9)",
                  "rgb(0, 255, 0, 1.0)"

                ],

              }, {
                label: "Precio máximo",
                data: max,
                backgroundColor: [

                  "rgb(0, 153, 204, 0.7)",
                  "rgb(0, 153, 204, 0.8)",
                  "rgb(0, 153, 204, 0.9)",
                  "rgb(0, 153, 204, 1.0)"

                ],

              }

            ]
          };




        });

        this.indicadorService.getHistoricoHotel(this.idDestino).then(data => {
          var label = [];
          var categoryData = [];
          var dataset = [];
          this.historicoHotelChart = [];
          //arreglo de arreglo para guardar los datos por categoria= 
          for (var i = 0; i < 6; i++) {
            label[i] = [];
            dataset[i] = [];
            categoryData[i] = {
              min: [],
              med: [],
              max: []

            };
          }


          var historicoLabels = {
            min: "Precio minimo",
            med: "Precio medio",
            max: "Precio máximo"

          };




          for (var i = 0; i < data.length; i++) {

            ///validar categoria
            var date = new Date(data[i].fecha);
            var cat = data[i].category;

            label[cat].push(date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());

            categoryData[cat].min.push(Math.floor(data[i].min * 100) / 100);
            categoryData[cat].med.push(Math.floor(data[i].medium * 100) / 100);
            categoryData[cat].max.push(Math.floor(data[i].max * 100) / 100);

          }

          /////

          for (var i = 0; i < 6; i++) {

            for (var key in historicoLabels) {

              if (historicoLabels.hasOwnProperty(key)) {

                var color = '#' + Math.random().toString(16).substr(-6);

                var set = {
                  label: historicoLabels[key],
                  fill: false,
                  data: categoryData[i][key],
                  pointColor: color, strokeColor: color,
                  backgroundColor: color,
                  borderColor: color,
                  borderWidth: 0.5
                };

                dataset[i].push(set);

              }


            }

            //agrega grafica por categoria
            let grafica = {
              labels: label[i],
              datasets: dataset[i]
            };


            this.historicoHotelChart.push(grafica);


          }


          //////
        });

        this.indicadorService.getHistoricoRestaurant(this.idDestino).then(data => {
          var label = [];
          var categoryDataRest = [];
          var dataset = [];
          this.historicoRestChart = [];
          //arreglo de arreglo para guardar los datos por categoria= 
          for (var i = 0; i < 4; i++) {
            label[i] = [];
            dataset[i] = [];
            categoryDataRest[i] = {
              min: [],
              med: [],
              max: []
            };
          }


          var historicoLabels = {
            min: "Precio minimo",
            med: "Precio medio",
            max: "Precio máximo"
          };




          for (var i = 0; i < data.length; i++) {

            ///validar categoria
            var date = new Date(data[i].fecha);



            var cat = data[i].category;

            label[cat].push(date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());


            categoryDataRest[cat].min.push(Math.floor(data[i].min * 100) / 100);
            categoryDataRest[cat].med.push(Math.floor(data[i].medium * 100) / 100);
            categoryDataRest[cat].max.push(Math.floor(data[i].max * 100) / 100);
          }

          /////

          for (var i = 0; i < 4; i++) {

            for (var key in historicoLabels) {

              if (historicoLabels.hasOwnProperty(key)) {

                var color = '#' + Math.random().toString(16).substr(-6);

                var set = {
                  label: historicoLabels[key],
                  fill: false,
                  data: categoryDataRest[i][key],
                  pointColor: color, strokeColor: color,
                  backgroundColor: color,
                  borderColor: color,
                  borderWidth: 0.5
                };

                dataset[i].push(set);

              }


            }

            //agrega grafica por categoria
            let grafica = {
              title: this.categoriaRest[i],
              labels: label[i],
              datasets: dataset[i]
            };

            this.historicoRestChart.push(grafica);

          }

          //////
        });
      }



    });

  }



  constructor(private router: ActivatedRoute, private indicadorService: IndicadorService, private elementRef: ElementRef) {
    this.idDestino = router.snapshot.params['id'];
  }

  rand(min, max) {
    return min + Math.random() * (max - min);
  }

  get_random_color() {
    var r = this.rand(255, 50);
    var g = this.rand(255, 50);
    var b = this.rand(255, 50);
    return 'rgba(' + r + ',' + g + ',' + b + ', 0.5)';
  }

  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

}