import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InicioComponent } from '../components/inicio.component';
import { ConfiguracionComponent } from '../components/configuracion.component';
import { UsuariosComponent } from '../components/usuarios.component';
import { AplicacionComponent } from '../components/aplicacion.component';
import { IndicadorComponent } from '../components/indicadores.component';
import { LocalidadComponent } from '../components/localidades.component';
import { ServiciosComponent } from '../components/servicios.component';
import { VisitantesComponent } from '../components/visitantes.component';
import { ComentariosComponent } from '../components/comentarios.component';
import { ReporteComponent } from '../components/reporte.component';

const routes: Routes = [
  { path: '', redirectTo: '/inicio', pathMatch: 'full' },
  { path: 'inicio', component: InicioComponent },

  { path: 'config/configuracion', component: ConfiguracionComponent },
  { path: 'config/usuarios', component: UsuariosComponent },
  { path: 'config/aplicacion', component: AplicacionComponent },

  { path: 'indicadores', component: IndicadorComponent },
  { path: 'localidades', component: LocalidadComponent },
  { path: 'localidades/:id', component: LocalidadComponent },
  { path: 'servicios', component: ServiciosComponent },
  { path: 'servicios/:id', component: ServiciosComponent },
  { path: 'visitantes', component: VisitantesComponent },
  { path: 'visitantes/:id', component: VisitantesComponent },
  { path: 'comentarios', component: ComentariosComponent },
  { path: 'comentarios/:id', component: ComentariosComponent },

 { path: 'reporte/:id', component: ReporteComponent }

];
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }