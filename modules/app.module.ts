import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { AppRoutingModule } from '../modules/app-routing.module';
//import 'chart.js/src/chart.js';
 
 import { IndicadorService } from '../services/indicadores.service';

 //import { ChartsModule } from 'ng2-charts';

import { ConfigService }          from '../services/config.service';

import { AppComponent }         from '../components/app.component';
import { InicioComponent }   from '../components/inicio.component';
import { ConfiguracionComponent }   from '../components/configuracion.component';
import { UsuariosComponent }   from '../components/usuarios.component';
import { AplicacionComponent }   from '../components/aplicacion.component';
import { IndicadorComponent }   from '../components/indicadores.component';
import { LocalidadComponent }   from '../components/localidades.component';
import { ServiciosComponent }   from '../components/servicios.component';
import { VisitantesComponent }   from '../components/visitantes.component';
import { ComentariosComponent }   from '../components/comentarios.component';
 import { ReporteComponent } from '../components/reporte.component';
import { ChartModule } from 'angular2-chartjs';
 

import { FilterPipe } from '../pipes/filter.pipe';
import { FilterPipeDestino } from '../pipes/filterDestino.pipe';
import { AppFilterPipe } from '../pipes/appFilterPipe.pipe';
 import { StringSort } from '../pipes/stringSort.pipe';

import { OrderBy } from '../pipes/sort.pipe';
import { unixTimeStamp } from '../pipes/date.pipe';
import { sortBy } from '../pipes/numericalSort.pipe';
import { CapitalizePipe } from '../pipes/caps.pipe';
import { MyDatePickerModule } from 'mydatepicker';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
     ChartModule,
    AppRoutingModule,
    MyDatePickerModule
  ],
  declarations: [
      AppComponent,
      OrderBy,
      InicioComponent,
      ConfiguracionComponent,
      UsuariosComponent,
      AplicacionComponent,
      IndicadorComponent,
      LocalidadComponent,
      ServiciosComponent,
      VisitantesComponent,
      ComentariosComponent,
      FilterPipe,
      OrderBy,
      sortBy,StringSort,
      FilterPipeDestino,
      AppFilterPipe,
      CapitalizePipe,
      unixTimeStamp,
      ReporteComponent
  ],  
  providers: [ ConfigService,IndicadorService ],
 
  bootstrap: [ AppComponent ]
})
export class AppModule { }