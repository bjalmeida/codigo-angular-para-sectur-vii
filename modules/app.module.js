"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var app_routing_module_1 = require('../modules/app-routing.module');
//import 'chart.js/src/chart.js';
//import { ChartsModule } from 'ng2-charts';
var config_service_1 = require('../services/config.service');
var app_component_1 = require('../components/app.component');
var inicio_component_1 = require('../components/inicio.component');
var configuracion_component_1 = require('../components/configuracion.component');
var usuarios_component_1 = require('../components/usuarios.component');
var aplicacion_component_1 = require('../components/aplicacion.component');
var indicadores_component_1 = require('../components/indicadores.component');
var localidades_component_1 = require('../components/localidades.component');
var servicios_component_1 = require('../components/servicios.component');
var visitantes_component_1 = require('../components/visitantes.component');
var comentarios_component_1 = require('../components/comentarios.component');
var sort_pipe_1 = require('../pipes/sort.pipe');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                //   ChartsModule,
                app_routing_module_1.AppRoutingModule
            ],
            declarations: [
                app_component_1.AppComponent,
                sort_pipe_1.OrderBy,
                inicio_component_1.InicioComponent,
                configuracion_component_1.ConfiguracionComponent,
                usuarios_component_1.UsuariosComponent,
                aplicacion_component_1.AplicacionComponent,
                indicadores_component_1.IndicadorComponent,
                localidades_component_1.LocalidadComponent,
                servicios_component_1.ServiciosComponent,
                visitantes_component_1.VisitantesComponent,
                comentarios_component_1.ComentariosComponent
            ],
            providers: [config_service_1.ConfigService],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map