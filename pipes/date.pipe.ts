import {Pipe} from "@angular/core";
import {PipeTransform} from "@angular/core";

@Pipe({name: 'unixTimeStamp'})
export class unixTimeStamp implements PipeTransform {

    transform(value:any) {
        if (value) {
            var today = value;
            var date = new Date(today);
          
            return date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() +"  "+ date.getHours()+":"+date.getMinutes() +":"+date.getSeconds();
        }
        return value;
    }

}