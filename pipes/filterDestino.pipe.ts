import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filterDestino'
})
export class FilterPipeDestino implements PipeTransform{
  transform(items: any[], args: number): any {
 
        if(args !== undefined){        
          return items.filter(item => item.tipoDestino == args);
        }else
        return items;
    }
}

//   return items.filter(item => item.leagueName.indexOf(args.leagueName) > -1);