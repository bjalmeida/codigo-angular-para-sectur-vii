import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filterItem'
})
export class FilterPipe implements PipeTransform{
  transform(items: any[], args: any[]): any {
 
        if(args !== undefined){        
          return items.filter(item => item.destino.toLowerCase().indexOf(args) > -1);
        }else
        return items;
    }
}

//   return items.filter(item => item.leagueName.indexOf(args.leagueName) > -1);