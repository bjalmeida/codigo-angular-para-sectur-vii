import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appFilter',
  pure: false
})
 
 

export class AppFilterPipe implements PipeTransform {
// Single Argument Filter
  transform(values: any[], arg1: any, arg2: any): any {

    if(arg1==undefined || arg2==undefined || arg2=="Todas las fuentes")
    return values;

    return values.filter(value => value[arg1] == arg2);
  }
}