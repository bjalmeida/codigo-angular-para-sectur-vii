import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'orderBy', pure: false})

export class OrderBy  implements PipeTransform{
  transform(array: Array<string>, args: string): Array<string> {
 
if(array==undefined)
  return;
  
    array.sort((a: any, b: any) => {

      var a1,b1;

      if(a.localidad==null || a.localidad==undefined)
        a1=a.municipioNombre;
        else
        a1=a.localidad;

        if(b.localidad==null || b.localidad==undefined)
        b1=b.municipioNombre;
        else
        b1=b.localidad;
  
 
        
      if (a1.toLowerCase() < b1.toLowerCase()) {
        return -1;
      } else if (a1.toLowerCase() > b1.toLowerCase()) {
        return 1;
      } else {

        return 0;
      }
    });
    return array;
  }

}


 