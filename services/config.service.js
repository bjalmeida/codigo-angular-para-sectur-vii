"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/Rx');
require('rxjs/add/operator/toPromise');
var ConfigService = (function () {
    function ConfigService(http) {
        this.http = http;
        this.configChanged = new core_1.EventEmitter();
        this.usersChanged = new core_1.EventEmitter();
        this.apiUrl = '/secturv2/rest/api/1.0'; // URL to web api
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
    }
    ConfigService.prototype.getAllLocations = function () {
        var _this = this;
        return this.http.get(this.apiUrl + "/getConfigData")
            .map(function (response) { return response.json(); })
            .subscribe(function (data) {
            _this.localidades = data;
            _this.configChanged.emit(_this.localidades);
        });
    };
    ConfigService.prototype.updateLocation = function (location) {
        var params = new http_1.URLSearchParams();
        params.set('object', JSON.stringify(location));
        return this.http.get(this.apiUrl + "/updateConfigData", { search: params }).toPromise();
    };
    ConfigService.prototype.deleteLocation = function (ids) {
        var params = new http_1.URLSearchParams();
        params.set('id', JSON.stringify(ids));
        return this.http.get(this.apiUrl + "/deleteConfigData", { search: params }).toPromise();
    };
    ConfigService.prototype.createLocation = function (location) {
        var params = new http_1.URLSearchParams();
        params.set('object', JSON.stringify(location));
        return this.http.get(this.apiUrl + "/newConfigData", { search: params }).toPromise();
    };
    /*Para admin de usuarios*/
    ConfigService.prototype.getAllUsers = function () {
        var _this = this;
        return this.http.get(this.apiUrl + "/getAllUsers")
            .map(function (response) { return response.json(); })
            .subscribe(function (data) {
            _this.usuarios = data;
            _this.usersChanged.emit(_this.usuarios);
        });
    };
    ConfigService.prototype.updateUser = function (usuario) {
        var params = new http_1.URLSearchParams();
        params.set('object', JSON.stringify(usuario));
        return this.http.get(this.apiUrl + "/updateUserData", { search: params }).toPromise();
    };
    ConfigService.prototype.deleteUser = function (ids) {
        var params = new http_1.URLSearchParams();
        params.set('id', JSON.stringify(ids));
        return this.http.get(this.apiUrl + "/deleteUserData", { search: params }).toPromise();
    };
    ConfigService.prototype.createUser = function (usuario) {
        var params = new http_1.URLSearchParams();
        params.set('object', JSON.stringify(usuario));
        return this.http.get(this.apiUrl + "/newUserData", { search: params }).toPromise();
    };
    ConfigService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ConfigService);
    return ConfigService;
}());
exports.ConfigService = ConfigService;
//# sourceMappingURL=config.service.js.map