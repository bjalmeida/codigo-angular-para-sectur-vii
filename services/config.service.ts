import { Injectable,EventEmitter } from '@angular/core';
import { Headers, Http,Response,URLSearchParams } from '@angular/http';
import { Configuracion } from '../class/Configuracion';
 import { Usuario } from '../class/Usuarios';
import 'rxjs/Rx'; 
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ConfigService {
 
configChanged = new EventEmitter<Configuracion[]>();
localidades:Configuracion[];
keywords:Configuracion[];

usersChanged = new EventEmitter<Usuario[]>();
usuarios:Usuario[];

private apiUrl = '/secturv2/rest/api/1.0';  // URL to web api
private headers = new Headers({'Content-Type': 'application/json'});

constructor(private http: Http) { }
 
/*locaciones administracion*/
  getAllLocations() {
    return this.http.get(this.apiUrl+"/getConfigData")
      .map((response: Response) => response.json())
      .subscribe(
        (data: Configuracion[]) => {
          this.localidades = data;
          this.configChanged.emit(this.localidades);
        }
      );
  }

   updateLocation(location:Configuracion) {
       let params = new URLSearchParams();    
       params.set('object', JSON.stringify(location));
       return this.http.get(this.apiUrl+"/updateConfigData",{ search: params }).toPromise();
  
   }

  deleteLocation(ids:string[]) {
    let params = new URLSearchParams();
     params.set('id', JSON.stringify(ids));
     return this.http.post(this.apiUrl+"/deleteConfigData", params).toPromise();
   
   }
   
   createLocation(location:Configuracion) {

    let params = new URLSearchParams();
    params.set('object', JSON.stringify(location));
      return this.http.post(this.apiUrl+"/newConfigData", params).toPromise();
   

   }
/*Para admin de usuarios*/

    getAllUsers() {
    return this.http.get(this.apiUrl+"/getAllUsers")
      .map((response: Response) => response.json())
      .subscribe(
        (data: Usuario[]) => {
          this.usuarios = data;
          this.usersChanged.emit(this.usuarios);
        }
      );
  }
   
 updateUser(usuario:Usuario) {
       let params = new URLSearchParams();    
       params.set('object', JSON.stringify(usuario));
       return this.http.get(this.apiUrl+"/updateUserData",{ search: params }).toPromise();
  
   }

  deleteUser(ids:string[]) {
    let params = new URLSearchParams();
     params.set('id', JSON.stringify(ids));
     return this.http.get(this.apiUrl+"/deleteUserData",{ search: params }).toPromise();
   
   }
   
   createUser(usuario:Usuario) {

    let params = new URLSearchParams();
    params.set('object', JSON.stringify(usuario));
      return this.http.get(this.apiUrl+"/newUserData",{ search: params }).toPromise();
   

   }


   /*gestion de indicadores y sus palabras claves*/

    getAllKeywords() {
    return this.http.get(this.apiUrl+"/getAllKeyWords")
      .map((response: Response) => response.json())
      .subscribe(
        (data: Configuracion[]) => {
          this.keywords = data;
          this.configChanged.emit(this.keywords);
        }
      );
  }

   updateAll(keyword:Configuracion[]) {
       let params = new URLSearchParams();    
       params.set('object', JSON.stringify(keyword));
       
       return this.http.post(this.apiUrl+"/updateKeyWord", params).toPromise();
  
   }

  deleteKeyword(ids:string[]) {
    let params = new URLSearchParams();
     params.set('id', JSON.stringify(ids));
     return this.http.get(this.apiUrl+"/deleteKeyWord",{ search: params }).toPromise();
   
   }
 /** obtienes estatus de los crawlers */
 getAppStatus(fecha:number) {
     
       let params = new URLSearchParams();
       params.set('start', JSON.stringify(fecha));
        return this.http.get(this.apiUrl + "/getAppStatus",{ search: params }).map((response: Response) => response.json()).toPromise();
    }
    
/*peticion solo recupera nombres del destino e ids para consulta posterior */
     getDestinos() {
        return this.http.get(this.apiUrl + "/getDestinos").map((response: Response) => response.json()).toPromise();
    }


      getLog(log: string) {

        let params = new URLSearchParams();
        params.set('log', JSON.stringify(log));
        return this.http.get(this.apiUrl + "/getLog", { search: params }).toPromise();
    }
}