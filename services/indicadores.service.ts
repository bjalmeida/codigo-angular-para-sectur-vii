import { Injectable, EventEmitter } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';

import 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class IndicadorService {



    private apiUrl = '/secturv2/rest/api/1.0';  // URL to web api
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    /*obtiene catalogo de indicadores */
    getIndicadoresCatalogo() {
        return this.http.get(this.apiUrl + "/getIndicadorCatalogo").map((response: Response) => response.json()).toPromise();
    }


    /*indicadores del dia */
    getIndicadoresData() {
        return this.http.get(this.apiUrl + "/getIndicadoresData").map((response: Response) => response.json()).toPromise();
    }

    /*consulta de los indicadores en rango de fechas */
    getIndicadoresDataFecha(inicio: any, fin: any) {
        let headers = new Headers({ 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' });

        let params = new URLSearchParams();
        params.set('start', JSON.stringify(inicio));
        params.set('end', JSON.stringify(fin));
        return this.http.post(this.apiUrl + "/getQueryByDate", params, { headers: headers }).map((response: Response) => response.json()).toPromise();
    }

    /*mejores locaciones del dia */
    gettodayBest() {
        return this.http.get(this.apiUrl + "/todayBest").map((response: Response) => response.json()).toPromise();
    }

    /* valoraciones de la mejores locaciones del dia a lo largo del mes*/
    getMonthBest() {
        return this.http.get(this.apiUrl + "/monthBest").map((response: Response) => response.json()).toPromise();
    }

    /*Valoraciones por destino */
    getDestinoValoraciones(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getDestinoValoraciones", { search: params }).map((response: Response) => response.json()).toPromise();
    }

    getLatlon(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getLatlon", { search: params }).map((response: Response) => response.json()).toPromise();
    }

    /* Hoteles y restaurantes */
    getHotelesEval(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getHotelesEval", { search: params }).map((response: Response) => response.json()).toPromise();
    }

    getHistoricoHotel(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getHistoricoHotel", { search: params }).map((response: Response) => response.json()).toPromise();
    }


    getRestaurantsEval(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getRestaurantsEval", { search: params }).map((response: Response) => response.json()).toPromise();
    }

    getHistoricoRestaurant(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getHistoricoRestaurant", { search: params }).map((response: Response) => response.json()).toPromise();
    }

    /** comentarios  */
    getComments(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getComments", { search: params }).map((response: Response) => response.json()).toPromise();
    }

    setFave(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/setFavorite", { search: params }).toPromise();
    }

    setRead(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/setRead", { search: params }).toPromise();
    }

    setDeleted(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/setDeleted", { search: params }).toPromise();
    }

    setPositive(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/setPositive", { search: params }).toPromise();
    }

    setNegative(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/setNegative", { search: params }).toPromise();
    }

    setNeutral(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/setNeutral", { search: params }).toPromise();
    }

     getCommentByDate(id: string,inicio: any, fin: any) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        params.set('start', JSON.stringify(inicio));
        params.set('end', JSON.stringify(fin));
        return this.http.get(this.apiUrl + "/getCommentByDate", { search: params }).map((response: Response) => response.json()).toPromise();
    }

 /*fotos */
  getFotos(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getFotos", { search: params }).map((response: Response) => response.json()).toPromise();
    }

/**viajeros */

 getViajeros(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getViajeros", { search: params }).map((response: Response) => response.json()).toPromise();
    }


     getTravelersAge(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getTravelersAge", { search: params }).map((response: Response) => response.json()).toPromise();
    }


     getTravelersGender(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getTravelersGender", { search: params }).map((response: Response) => response.json()).toPromise();
    }

      getWordCloud(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getWordCloud", { search: params }).map((response: Response) => response.json()).toPromise();
    }

     getTravelersCity(id: string) {

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
        return this.http.get(this.apiUrl + "/getTravelersCity", { search: params }).map((response: Response) => response.json()).toPromise();
    }


 getHistorico(field: string) {

        let params = new URLSearchParams();
        params.set('field', JSON.stringify(field));
        return this.http.get(this.apiUrl + "/getHistorico", { search: params }).map((response: Response) => response.json()).toPromise();
    }
/**Reporte */

 getReporte(id:any) {
        let headers = new Headers({ 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' });

        let params = new URLSearchParams();
        params.set('id', JSON.stringify(id));
    
        return this.http.post(this.apiUrl + "/getReporte", params, { headers: headers }).map((response: Response) => response.json()).toPromise();
    }

    getBestType(tipo:any) {
        let headers = new Headers({ 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' });

        let params = new URLSearchParams();
        params.set('type', JSON.stringify(tipo));
    
        return this.http.post(this.apiUrl + "/getBestType", params, { headers: headers }).map((response: Response) => response.json()).toPromise();
    }

   
}